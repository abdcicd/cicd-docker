<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use Tests\RefreshTestState;
use Illuminate\Support\Str;
use Laravel\Passport\Passport;
use App\Models\Permission;
use Illuminate\Routing\Middleware\ThrottleRequests;
use Illuminate\Foundation\Testing\RefreshDatabase ;

class Base extends TestCase
{
    // use RefreshDatabase;

    public $connectionsToTransact = ['pgsql'];
    protected $path;
    
    protected $profileStructure;
    protected $singleStructure;
    protected $indexStructure;
    protected $modelName;
    protected $modelNamePlural;
    protected $modelClass;
    protected $paramName;
    protected $user;
    protected $rawData;
    protected $orgDbName;
    protected $hasTranslation = true;
    protected $modelTranslationClass;
    protected $relatedModels = [];
    protected $commonResponseStructure;
    protected $apiUrl;

    public function setUp() : void
    {
        parent::setUp();

        // $this->modelClass::registerEvents();

        $this->modelName = strtolower(str_replace("App\Models\\", "", $this->modelClass));
        
        $this->modelNamePlural = Str::plural($this->modelName);

        $this->paramName = Str::snake(Str::camel($this->modelName));

        $this->user = User::firstWhere('username', 'AN0657');
        
        $this->path = config('app.tested_routes_file');

        if (! RefreshTestState::$started) {

            if(file_exists($this->path)){
                unlink($this->path);
            }

            RefreshTestState::$started = true;
        }

        $this->commonResponseStructure = [
            'status',
            'message',
            'data'
        ];

        // disable Observers
        Permission::unsetEventDispatcher();

        $this->withoutMiddleware(
            ThrottleRequests::class
        );

        $this->apiUrl = 'admin.' . $this->modelName;

    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }


    protected function getHeadersWithToken(){

        Passport::actingAs($this->user);
        $token = $this->user->createToken('Test token')->accessToken;
        return ['Authorization' => 'Bearer '. $token];
    }

    protected function route($name, $parameters = [], $absolute = true)
    {
        $this->writeToTestedRoutesFile($name);
        return route($name, $parameters, $absolute);
    }

    protected function writeToTestedRoutesFile($name)
    {
        $this->path = 'tests/tested-routes.json';
        $data = [];
        if(file_exists($this->path)){
            $data = json_decode(file_get_contents($this->path));
        }
        $data[] = $name;
        file_put_contents($this->path, json_encode($data));
    }

    protected function createModels($count = 1){

        // $params = [
        //     'created_by' => $this->user->id,
        //     'updated_by' => $this->user->id
        // ];

        // $models = $this->modelClass::factory($count)->create(!$params);
        
        // if($this->hasTranslation){
        //     $models->each(function ($model) {
        //         $model->translationClass::factory()->create(['object_id' => $model->id, 'language_code' => 'uz']);
        //         $model->translationClass::factory()->create(['object_id' => $model->id, 'language_code' => 'en']);
        //         $model->translationClass::factory()->create(['object_id' => $model->id, 'language_code' => 'ru']);
        //     });
        // }

        // return $models;
    }

}
