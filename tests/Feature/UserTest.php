<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\Feature\Base;

class UserTest extends Base
{
    public function test_can__index___200()
    {
        $parameters = [];
        $structure = ["data" => ["*" => ["roles" => ["*" => "superadmin"], "permissions" => [], "0" => "id", "1" => "uuid", "2" => "username", "3" => "firstname", "4" => "lastname"]], "links" => ["first", "last", "prev", "next"], "meta" => ["links" => ["*" => ["url", "label", "active"]], "0" => "current_page", "1" => "from", "2" => "last_page", "3" => "path", "4" => "per_page", "5" => "to", "6" => "total"]];

        $this->getJson($this->route('master.user.index', $parameters), $this->getHeadersWithToken())
            ->assertJsonStructure($structure)
            ->assertStatus(200);
    }

    public function test_can__index___401()
    {
        $parameters = [];
        $this->getJson($this->route('master.user.index', $parameters), [])
            ->assertStatus(401);
    }

    public function test_can__index___404()
    {
        $parameters = [];
        $this->getJson($this->route('master.user.index', $parameters), $this->getHeadersWithToken())
            ->assertStatus(404);
    }

    public function test_can__store___201()
    {
        $data = ["username" => "9g6eJj8itv", "firstname" => "XSEDme891l", "lastname" => "y5t15Al6JP"];
        $this->postJson($this->route('master.user.store'), $data, $this->getHeadersWithToken())
            ->assertStatus(201);
    }

    public function test_can__store___422()
    {
        $data = ["username" => "9g6eJj8itv", "firstname" => "XSEDme891l", "lastname" => "y5t15Al6JP"];
        $this->postJson($this->route('master.user.store'), $data, $this->getHeadersWithToken())
            ->assertStatus(422);
    }
    public function test_can__show___200()
    {
        $parameters = ["user" => 1];
        $structure = ["trace" => ["*" => ["file", "line", "function", "class", "type"]], "0" => "message", "1" => "exception", "2" => "file", "3" => "line"];
        $fragment = ["message" => "Please give main operation column name App\\Repositories\\UserRepository=>=>$id constructor. Default column is id"];
        $this->getJson($this->route('master.user.show', $parameters), $this->getHeadersWithToken())
            ->assertJsonStructure($structure)
            ->assertJsonFragment($fragment)
            ->assertStatus(200);
    }

    public function test_can__show___401()
    {
        $parameters = ["user" => 1];
        $this->getJson($this->route('master.user.show', $parameters), [])
            ->assertStatus(401);
    }

    public function test_can__show___404()
    {
        $parameters = ["user" => 1];
        $this->getJson($this->route('master.user.show', $parameters), $this->getHeadersWithToken())
            ->assertStatus(404);
    }

    public function test_can__update___200()
    {
        $parameters = ["user" => 1];
        $data = ["username" => "CPEwEbxzEm", "firstname" => "wvftw3El0O", "lastname" => "Kv1RPcuMVo"];
        $this->putJson($this->route('master.user.update', $parameters), $data, $this->getHeadersWithToken())
            ->assertStatus(201);
    }

    public function test_can__update___422()
    {
        $parameters = ["user" => 1];
        $data = ["username" => "CPEwEbxzEm", "firstname" => "wvftw3El0O", "lastname" => "Kv1RPcuMVo"];
        $this->putJson($this->route('master.user.update', $parameters), $data, $this->getHeadersWithToken())
            ->assertStatus(422);
    }

    public function test_can__update___404()
    {
        $parameters = ["user" => 1];
        $data = ["username" => "CPEwEbxzEm", "firstname" => "wvftw3El0O", "lastname" => "Kv1RPcuMVo"];
        $this->putJson($this->route('master.user.update', $parameters), $data, $this->getHeadersWithToken())
            ->assertStatus(404);
    }
    public function test_can__destroy___200()
    {
        $parameters = ["user" => 1];
        $this->getJson($this->route('master.user.destroy', $parameters), $this->getHeadersWithToken())
            ->assertStatus(204);
    }

    public function test_can__destroy___401()
    {
        $parameters = ["user" => 1];
        $this->getJson($this->route('master.user.destroy', $parameters), [])
            ->assertStatus(401);
    }

    public function test_can__destroy___404()
    {
        $parameters = ["user" => 1];
        $this->getJson($this->route('master.user.destroy', $parameters), $this->getHeadersWithToken())
            ->assertStatus(404);
    }

    public function test_can__profile___200()
    {
        $parameters = [];
        $structure = ["data" => ["roles" => ["*" => "superadmin"], "permissions" => [], "0" => "id", "1" => "uuid", "2" => "username", "3" => "firstname", "4" => "lastname"]];
        $fragment = ["data" => ["id" => 1]];
        $this->getJson($this->route('master.user-profile', $parameters), $this->getHeadersWithToken())
            ->assertJsonStructure($structure)
            ->assertJsonFragment($fragment)
            ->assertStatus(200);
    }

    public function test_can__profile___401()
    {
        $parameters = [];
        $this->getJson($this->route('master.user-profile', $parameters), [])
            ->assertStatus(401);
    }

    public function test_can__profile___404()
    {
        $parameters = [];
        $this->getJson($this->route('master.user-profile', $parameters), $this->getHeadersWithToken())
            ->assertStatus(404);
    }
}
