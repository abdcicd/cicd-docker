<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::prefix('auth')->name('auth.')->group(base_path('routes/api/auth.php'));

Route::group(['middleware' => ['auth:api']], function () {

    Route::prefix('master')->name('master.')->group(base_path('routes/api/master.php'));

});
