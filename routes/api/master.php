<?php

use App\Http\Controllers as Clrs;
use Illuminate\Support\Facades\Route;

Route::apiResource('permission', Clrs\PermissionController::class)->only(['index', 'show', 'update']);
Route::apiResource('role', Clrs\RoleController::class);
Route::put('role-permissions-sync/{id}', [Clrs\RoleController::class, 'syncPermissions'])->name('role.sync_permissions');
Route::get('ad-users', [Clrs\AuthController::class, 'adUsers'])->name("ad-users");
Route::put('user-attach-role/{id}', [Clrs\AuthController::class, 'attachRole'])->name("user-attach-role");
Route::put('user-detach-role/{id}', [Clrs\AuthController::class, 'detachRole'])->name("user-detach-role");
Route::put('user-sync-role/{id}', [Clrs\AuthController::class, 'syncRoles'])->name("user-sync-roles");
Route::apiResource('user', Clrs\UserController::class);
Route::get('user-profile', [Clrs\UserController::class, 'profile'])->name('user-profile');
Route::apiResource('company', Clrs\CompanyController::class);
Route::apiResource('language', Clrs\LanguageController::class);
Route::apiResource('area', Clrs\AreaController::class);
Route::apiResource('grid', Clrs\GridController::class);
Route::apiResource('container', Clrs\ContainerController::class);
Route::get('container-matrix', [Clrs\ContainerController::class, 'matrix'])->name('container-matrix');
Route::get('container-out', [Clrs\ContainerController::class, 'outList'])->name('container-out');
Route::post('container-input', [Clrs\ContainerController::class, 'input'])->name('container-input');
Route::put('container-move/{container}', [Clrs\ContainerController::class, 'move'])->name('container-move');
Route::delete('container-output/{container}', [Clrs\ContainerController::class, 'output'])->name('container-output');
Route::post('container-file', [Clrs\ContainerController::class, 'uploadData'])->name('container-file');
Route::apiResource('action', Clrs\ActionController::class)->only('index');


Route::get('list-container-types', [Clrs\EnumsController::class, 'containerTypes'])->name('list-container-types');
Route::get('list-container-status-types', [Clrs\EnumsController::class, 'containerStatusTypes'])->name('list-container-status-types');
Route::get('list-company-types', [Clrs\EnumsController::class, 'companyTypes'])->name('list-company-types');