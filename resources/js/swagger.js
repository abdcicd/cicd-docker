import SwaggerUIBundle from 'swagger-ui';
import 'swagger-ui/dist/swagger-ui.css';

function preRequestHook(request) {
  request.headers.Authorization = 'Bearer ' + localStorage.getItem('accessToken');
  return request;
}
function completeHook(response) {
  let token = response.obj.access_token;
  if (token) {
    let authbtns = document.querySelectorAll(".authorization__btn");
    for (let btn of authbtns) {
      btn.classList.remove('unlocked')
    }
    localStorage.setItem('accessToken', token)
  }
  return response;
}


export const swagger = (url, dom_id) => SwaggerUIBundle({
  dom_id,
  url,
  presets: [
    SwaggerUIBundle.presets.apis,
    SwaggerUIBundle.SwaggerUIStandalonePreset
  ],
  requestInterceptor: preRequestHook,
  responseInterceptor: completeHook,
})