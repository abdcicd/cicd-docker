<p align="center"><a href="https://jobs-api-test.adm.uz/admin-api-docs" target="_blank"><img src="https://jobs-test.adm.uz/static/media/logo.0121155821dccc1f42f9.png" width="200" alt="Laravel Logo"></a></p>



## This project created for ADM Global. 


<h3><b>Tizimga bo'lgan talablar:</b></h3><br/>

    PHP: ^8.1*,
    Postgresql: ^15.*,
    Laravel: 10.*

<h3><b>O'rnatish qadamlari:</b></h3><br/>

1. <code>        Loyiha kodlarini gitlabdan yuklab oladi va "cy-api" papkaga joylaydi</code><br></br>

        git clone https://gitlab.com/admautdevs/adm/jobs/api.git cy-api
	
        
2. <code>Loyiha papkasiga o'tish</code><br></br>
	
        cd ./cy-api

        
3. <code>Composer paketlarni o'rnatish</code><br></br>
	
        composer update
    
        
4. <code>Config .env file yaratish</code><br></br>
        
        cmd - "type nul > .env"
        bash - "nano .env"
        - DB yarating va .env file ni update qiling

5. <code>Applicatation key yaratadi</code><br></br>
	
        php artisan key:generate

6. <code>DB ga table lar yaratadi</code><br></br>
	
        php artisan migrate:fresh
	
7. <code>Client ID va Client secret larni generate qiladi.</code><br></br>

	    php artisan passport:install --force
	
8. <code>Seederni ishga tushiradi. Shundan so'ng username, password, client id va client secret yordamida Bearer token olish mumkin.</code><br></br>
		
        php artisan db:seed

9. <code>Yuqoridagi artisan buyruqlarini hammasini qilishga erinsangiz quyidagi buyruq aynan siz uchun</code><br></br>

	    php artisan startup
      
        - key:generate
        - migrate:fresh
        - passport:install --force
        - db:seed
        - optimize
        - config:clear
        - update-permissions
	
10. <code>Loyihani run qilish</code><br></br>

	    php artisan serve --port=8000 --host=127.0.0.1

11. <code>Api endpointlarni ko'rish swagger orqali ko'rish uchun quyidagi linkka o'tish mumkin</code><br></br>
            
        http://127.0.0.1:8000/main-api-docs
<br><hr><br>