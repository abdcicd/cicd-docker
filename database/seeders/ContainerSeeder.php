<?php

namespace Database\Seeders;

use App\Enums\CompanyTypeEnum;
use App\Enums\ContainerStatusEnum;
use App\Enums\ContainerTypeEnum;
use App\Models\Area;
use App\Models\Company;
use App\Models\Container;
use App\Models\Grid;
use App\Models\Position;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class ContainerSeeder extends Seeder
{
    public function run()
    {
        $shippers = Company::randomBuilder([['type', '=', CompanyTypeEnum::SHIPPER]]);
        $forwarders = Company::randomBuilder([['type', '=', CompanyTypeEnum::FORWARDER]]);
        $consignees = Company::randomBuilder([['type', '=', CompanyTypeEnum::CONSIGNEE]]);

        $areas = Area::where('buffer', 0)->get();
        $c = 0;
        foreach ($areas as $area) {
            $grids = Grid::where('area_id', $area->id)->get();
            $count = $area->x * $area->y;
            for ($i = 1; $i <= $area->limit; $i++) {
                $start = (($i - 1) * $count) + 1;
                $end = $i * $count;
                for ($j = $start; $j <= $end; $j++) {
                    $c++;
                    $container = Container::create([
                        'number' => "$c-container",
                        'type' => ContainerTypeEnum::random(),
                        'status' => ContainerStatusEnum::random(),
                        'invoice_number' => Str::random(8),
                        'bl_number'=> Str::random(8),
                        'shipping_date' => now()->addDays($c),
                        'eta' => now()->addDays($c),
                        'ata' => now()->addDays($c),
                        'shipper_id' => $shippers->random()?->id ?? 1,
                        'forwarder_id' => $forwarders->random()?->id ?? 1,
                        'consignee_id' => $consignees->random()?->id ?? 1,
                    ]);
                    Position::create([
                        'grid_name' => $grids[$j - $start]->name,
                        'z_axis' => $i,
                        'container_id' => $container->id
                    ]);
                }
            }
        }
    }
}
