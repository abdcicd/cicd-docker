<?php

namespace Database\Seeders;

use App\Models\Area;
use App\Models\Grid;
use Illuminate\Database\Seeder;

class GridSeeder extends Seeder
{
    public function run()
    {
        $areas = Area::all();
        foreach ($areas as $area) {
            $x = $area->x;
            $y = $area->y;
            for ($j = 1; $j <= $y; $j++) {
                for ($i = 1; $i <= $x; $i++) {
                    Grid::create([
                        'name' => "$area->name.$i.$j",
                        'area_id' => $area->id,
                        'x_axis' => $i,
                        'y_axis' => $j,
                    ]);
                }
            }
        }
    }
}
