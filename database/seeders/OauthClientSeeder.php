<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OauthClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $oauth_client_secret = config('app.oauth-client-secret');
        DB::unprepared("UPDATE oauth_clients SET secret = '$oauth_client_secret' WHERE id = 2;");
    }
}
