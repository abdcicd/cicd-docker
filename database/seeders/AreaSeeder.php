<?php

namespace Database\Seeders;

use App\Models\Area;
use Illuminate\Database\Seeder;

class AreaSeeder extends Seeder
{
    public function run()
    {
        Area::create([
            'name' => 'A',
            'x' => 36,
            'y' => 10,
            'limit' => 3,
            'description' => 'A area'
        ]); 
        Area::create([
            'name' => 'B',
            'x' => 1,
            'y' => 2,
            'limit' => 3,
            'buffer' => 1,
            'description' => 'B area'
        ]); 
        Area::create([
            'name' => 'C',
            'x' => 36,
            'y' => 10,
            'limit' => 4,
            'description' => 'C bufer area'
        ]); 
    }
}
