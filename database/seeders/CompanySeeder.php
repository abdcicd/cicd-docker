<?php

namespace Database\Seeders;

use App\Enums\CompanyTypeEnum;
use App\Models\Company;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class CompanySeeder extends Seeder
{
    public function run()
    {
        Company::create([
            'name' => "ADM Global",
            'code' => "ADMGLOB",
            'type' => CompanyTypeEnum::random(),
            'created_by' => 1
        ]);
        Company::create([
            'name' => "ADM Global",
            'code' => "ROODELL",
            'type' => CompanyTypeEnum::random(),
            'created_by' => 1
        ]);
        Company::create([
            'name' => "Chery",
            'code' => Str::random(8),
            'type' => CompanyTypeEnum::random(),
            'created_by' => 1
        ]);
        Company::create([
            'name' => "BYD",
            'code' => Str::random(8),
            'type' => CompanyTypeEnum::random(),
            'created_by' => 1
        ]);
        Company::create([
            'name' => "Kia",
            'code' => Str::random(8),
            'type' => CompanyTypeEnum::random(),
            'created_by' => 1
        ]);
        Company::create([
            'name' => "GM Uz",
            'code' => Str::random(8),
            'type' => CompanyTypeEnum::random(),
            'created_by' => 1
        ]);
    }
}
