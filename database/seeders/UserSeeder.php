<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{

    protected $users = [
        [
            'username' => "AN0657",
            'firstname' => 'Abdumannon',
            'lastname' => 'Norboyev'
        ],
        [
            'username' => "ZB0513",
            'firstname' => 'Zafar',
            'lastname' => 'Badalboyev'
        ],
        [
            'username' => "FM0675",
            'firstname' => 'FAZLIDDIN',
            'lastname' => 'MIRZAQOSIMOV'
        ],
        [
            'username' => "MY0293",
            'firstname' => 'MUHAMMADRIZO',
            'lastname' => 'YULBARSBEKOV'
        ],
        [
            'username' => "SV0106",
            'firstname' => 'SOHIBJON',
            'lastname' => 'VALIYEV'
        ],
        [
            'username' => "DB0010",
            'firstname' => 'DILSHODBEK',
            'lastname' => 'BOYMIRZAEV'
        ],
        [
            'username' => "OA0058",
            'firstname' => 'UKTAM',
            'lastname' => 'ACHILOV'
        ],
        [
            'username' => "SM0141",
            'firstname' => 'SHERALI',
            'lastname' => 'MAMATKARIMOV'
        ]
    ];
    public function run()
    {
        foreach ($this->users as $user) {
            $u = $this->createUser($user);
            $u->roles()->attach('superadmin');
        }
    }

    protected function createUser($data)
    {
        return User::create([
            'username' => $data['username'],
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
        ]);
    }

    protected static function string($count)
    {
        do {
            $token = Str::random($count);
        } while (User::where("verification_token", "=", $token)->first());

        return $token;
    }
}
