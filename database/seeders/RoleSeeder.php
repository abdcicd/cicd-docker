<?php

namespace Database\Seeders;

use App\Models\Language;
use App\Models\Role;
use Illuminate\Database\Seeder;
use App\Enums\PermissionTypeEnum;
use App\Models\RoleTranslation;

class RoleSeeder extends Seeder
{
    public function run()
    {
        $superadmin = Role::create([
            'name' => 'superadmin',
            'type' => PermissionTypeEnum::PERMISSION_TYPE_SUPERADMIN
        ]);

        $languages = Language::where('is_active', true)->get();
        
        foreach ($languages as $language) {
            RoleTranslation::create([
                'object_id' => $superadmin->id,
                'language_code' => $language->code,
                'translation' => 'Super admin',
                'description' => 'Super admin',
            ]);
        }
    }
}
