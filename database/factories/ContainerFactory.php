<?php

namespace Database\Factories;

use App\Enums\ContainerStatusEnum;
use App\Enums\ContainerTypeEnum;
use App\Models\Company;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class ContainerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $companyRandom = Company::randomBuilder();
        return [
            'number' => $this->faker->word(),
            'type' => ContainerTypeEnum::random(),
            'status' => ContainerStatusEnum::random(),
            'invoice_number' => $this->faker->word(),
            'bl_number' => $this->faker->word(),
            'shipping_date' => now(),
            'eta' => now(),
            'ata' => now(),
            'shipper_id' => $companyRandom->random()?->id,
            'forwarder_id' => $companyRandom->random()?->id,
            'consignee_id' => $companyRandom->random()?->id,
            'created_by' => $this->faker->randomElement(User::all()->pluck('id')),
        ];
    }
}
