<?php

namespace Database\Factories;

use App\Enums\CompanyTypeEnum;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class CompanyFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word(),
            'code' => $this->faker->word(),
            'type' => CompanyTypeEnum::random(),
            'created_by' => $this->faker->randomElement(User::all()->pluck('id')),
        ];
    }
}
