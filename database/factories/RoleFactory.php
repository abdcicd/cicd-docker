<?php

namespace Database\Factories;

use App\Enums\PermissionTypeEnum;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class RoleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->unique()->name(),
            'type' => $this->faker->randomElement(PermissionTypeEnum::list()),
        ];
    }
}
