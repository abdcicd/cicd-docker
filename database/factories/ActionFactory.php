<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class ActionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'container_number' => $this->faker->word(),
            'container_type' => $this->faker->word(),
            'container_status' => $this->faker->word(),
            'shipper_name' => $this->faker->word(),
            'forwarder_name' => $this->faker->word(),
            'consignee_name' => $this->faker->word(),
            'user_name' => $this->faker->word(),
            'action' => $this->faker->word(),
            'position' => $this->faker->word(),
            'old_position' => $this->faker->word(),
            'created_at' => now(),
            'created_by' => 1,
        ];
    }

    protected function s($i)
    {
        return Str::random($i);
    }
}
