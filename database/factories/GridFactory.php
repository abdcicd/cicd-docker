<?php

namespace Database\Factories;

use App\Models\Area;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class GridFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word(),
            'area_id' => Area::first()?->id ?? 1,
            'x_axis' => 1,
            'y_axis' => 1,
            'created_by' => $this->faker->randomElement(User::all()->pluck('id')),
        ];
    }
}
