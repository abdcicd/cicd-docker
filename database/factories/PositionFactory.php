<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class PositionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'grid_id' => 1,
            'z_axis' => 1,
            'status' => 10,
            'state' => 10,
            'container_number' => "Aa",
            'created_by' => auth()->id(),
            'updated_by' => auth()->id(),
        ];
    }
}
