<?php

namespace Database\Factories;

use App\Models\Role;
use App\Models\Language;
use Illuminate\Database\Eloquent\Factories\Factory;

class RoleTranslationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'object_id' => $this->faker->randomElement(Role::all()->pluck('id')),
            'translation' => $this->faker->word(),
            'description' => $this->faker->word(),
            'language_code' => $this->faker->randomElement(Language::all()->pluck('code')),
        ];
    }
}
