<?php

namespace Database\Factories;
use Illuminate\Support\Str;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class AreaFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word(),
            'limit' => $this->faker->randomDigit(),
            'x' => 200,
            'y' => 6,
            'description' => Str::random(20),
            'created_by' => $this->faker->randomElement(User::all()->pluck('id')),
        ];
    }
}
