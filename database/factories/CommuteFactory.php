<?php

namespace Database\Factories;

use App\Models\Company;
use App\Models\Container;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class CommuteFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word(),
            'shipper_id' => $this->faker->randomElement(Company::all()->pluck('id')),
            'forwarder_id' => $this->faker->randomElement(Company::all()->pluck('id')),
            'container_id' => $this->faker->randomElement(Container::all()->pluck('id')),
            'arrived_time' => now(),
            'gone_time' => null,
            'due_time' => now(),
            'created_by' => $this->faker->randomElement(User::all()->pluck('id')),
        ];
    }
}
