<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContainersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('containers', function (Blueprint $table) {
            $table->id();
            $table->string('number')->unique();
            $table->integer('type')->default(20);
            $table->integer('status')->default(10);
            $table->text('invoice_number')->nullable();
            $table->text('bl_number')->nullable();
            $table->timestamp('shipping_date')->nullable();
            $table->timestamp('eta')->nullable();
            $table->timestamp('ata')->nullable();
            $table->bigInteger('shipper_id')->nullable();
            $table->bigInteger('forwarder_id')->nullable();
            $table->bigInteger('consignee_id')->nullable();
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('updated_by')->nullable();
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('containers');
    }
}
