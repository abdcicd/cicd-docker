<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actions', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('container_id');
            $table->text('container_number');
            $table->text('container_status');
            $table->text('container_type')->nullable();
            $table->text('shipper_name')->nullable();
            $table->text('forwarder_name')->nullable();
            $table->text('consignee_name')->nullable();
            $table->text('user_name')->nullable();
            $table->text('action');
            $table->text('position')->nullable();
            $table->text('old_position')->nullable();
            $table->bigInteger('created_by')->nullable();
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actions');
    }
}
