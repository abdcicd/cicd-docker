<?php

use App\Models\Permission;
use App\Models\Role;

 return array (
  'master.role.index' => 
  array (
    'uri' => 'api/master/role',
    'name' => 'master.role.index',
    'prefix' => 'api/master',
    'folder' => 'master/role',
    'action' => 'index',
    'method' => 'GET',
    'tags' => 
    array (
      0 => 'role',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
  'master.role.store' => 
  array (
    'uri' => 'api/master/role',
    'name' => 'master.role.store',
    'prefix' => 'api/master',
    'folder' => 'master/role',
    'action' => 'store',
    'method' => 'POST',
    'data' => 
    array (
      'name' => 'admins1',
      'type' => 2,
      'translations' => [
        [
          'translation' => 'a',
          'description' => 'a',
          'language_code' => 'uz'
        ]
      ]
    ),
    'tags' => 
    array (
      0 => 'role',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
  'master.role.show' => 
  array (
    'uri' => 'api/master/role/{role}',
    'name' => 'master.role.show',
    'prefix' => 'api/master',
    'folder' => 'master/role',
    'action' => 'show',
    'method' => 'GET',
    'parameters' => 
    array (
      'params' => 
      array (
        'role' => function () {
          return Role::first()?->id ?? 1;
        },
      ),
      'infos' => 
      array (
        'role' => 
        array (
          'in' => 'path',
          'value' => 1,
        ),
      ),
    ),
    'tags' => 
    array (
      0 => 'role',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
  'master.role.update' => 
  array (
    'uri' => 'api/master/role/{role}',
    'name' => 'master.role.update',
    'prefix' => 'api/master',
    'folder' => 'master/role',
    'action' => 'update',
    'method' => 'PUT',
    'parameters' => 
    array (
      'params' => 
      array (
        'role' => function () {
          return Role::first()?->id ?? 1;
        },
      ),
      'infos' => 
      array (
        'role' => 
        array (
          'in' => 'path',
          'value' => 1,
        ),
      ),
    ),
    'data' => 
    array (
      'name' => 'superaaaasa',
      'type' => 2,
      'translations' => [
        [
          'translation' => 'a',
          'description' => 'a',
          'language_code' => 'uz'
        ]
      ]
    ),
    'tags' => 
    array (
      0 => 'role',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
  'master.role.destroy' => 
  array (
    'uri' => 'api/master/role/{role}',
    'name' => 'master.role.destroy',
    'prefix' => 'api/master',
    'folder' => 'master/role',
    'action' => 'destroy',
    'method' => 'DELETE',
    'parameters' => 
    array (
      'params' => 
      array (
        'role' => function () {
          return Role::first()?->id ?? 3;
        },
      ),
      'infos' => 
      array (
        'role' => 
        array (
          'in' => 'path',
          'value' => 1,
        ),
      ),
    ),
    'tags' => 
    array (
      0 => 'role',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
  'master.role.sync_permissions' => 
  array (
    'uri' => 'api/master/role-permissions-sync/{id}',
    'name' => 'master.role.sync_permissions',
    'prefix' => 'api/master',
    'folder' => 'master/role',
    'action' => 'syncPermissions',
    'method' => 'PUT',
    'parameters' => 
    array (
      'params' => 
      array (
        'id' => 1,
      ),
      'infos' => 
      array (
        'id' => 
        array (
          'in' => 'path',
          'value' => 1,
        ),
      ),
    ),
    'data' => 
    array (
      ['permissions' => Permission::select('name')->get()->toArray()]
    ),
    'tags' => 
    array (
      0 => 'role',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
);