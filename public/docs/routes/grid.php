<?php 
 return array (
  'master.grid.index' => 
  array (
    'uri' => 'api/master/grid',
    'name' => 'master.grid.index',
    'prefix' => 'api/master',
    'folder' => 'master/grid',
    'action' => 'index',
    'method' => 'GET',
    'tags' => 
    array (
      0 => 'grid',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
  'master.grid.store' => 
  array (
    'uri' => 'api/master/grid',
    'name' => 'master.grid.store',
    'prefix' => 'api/master',
    'folder' => 'master/grid',
    'action' => 'store',
    'method' => 'POST',
    'data' => 
    array (
      'name' => 'Maydon 2',
      'area_id' => 2,
      'x_axis' => 2,
      'y_axis' => 3,
    ),
    'tags' => 
    array (
      0 => 'grid',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
  'master.grid.show' => 
  array (
    'uri' => 'api/master/grid/{grid}',
    'name' => 'master.grid.show',
    'prefix' => 'api/master',
    'folder' => 'master/grid',
    'action' => 'show',
    'method' => 'GET',
    'parameters' => 
    array (
      'params' => 
      array (
        'grid' => "A.1.1",
      ),
      'infos' => 
      array (
        'grid' => 
        array (
          'in' => 'path',
          'value' => "A.1.1",
        ),
      ),
    ),
    'tags' => 
    array (
      0 => 'grid',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
  'master.grid.update' => 
  array (
    'uri' => 'api/master/grid/{grid}',
    'name' => 'master.grid.update',
    'prefix' => 'api/master',
    'folder' => 'master/grid',
    'action' => 'update',
    'method' => 'PUT',
    'parameters' => 
    array (
      'params' => 
      array (
        'grid' => 2,
      ),
      'infos' => 
      array (
        'grid' => 
        array (
          'in' => 'path',
          'value' => 2,
        ),
      ),
    ),
    'data' => 
    array (
      'name' => 'Maydon 1',
      'area_id' => 2,
      'x_axis' => 1,
      'y_axis' => 1,
    ),
    'tags' => 
    array (
      0 => 'grid',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
  'master.grid.destroy' => 
  array (
    'uri' => 'api/master/grid/{grid}',
    'name' => 'master.grid.destroy',
    'prefix' => 'api/master',
    'folder' => 'master/grid',
    'action' => 'destroy',
    'method' => 'DELETE',
    'parameters' => 
    array (
      'params' => 
      array (
        'grid' => 1,
      ),
      'infos' => 
      array (
        'grid' => 
        array (
          'in' => 'path',
          'value' => 1,
        ),
      ),
    ),
    'tags' => 
    array (
      0 => 'grid',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
);