<?php 
 return array (
  'master.permission.index' => 
  array (
    'uri' => 'api/master/permission',
    'name' => 'master.permission.index',
    'prefix' => 'api/master',
    'folder' => 'master/permission',
    'action' => 'index',
    'method' => 'GET',
    'tags' => 
    array (
      0 => 'permission',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
  'master.permission.show' => 
  array (
    'uri' => 'api/master/permission/{permission}',
    'name' => 'master.permission.show',
    'prefix' => 'api/master',
    'folder' => 'master/permission',
    'action' => 'show',
    'method' => 'GET',
    'parameters' => 
    array (
      'params' => 
      array (
        'permission' => 8,
      ),
      'infos' => 
      array (
        'permission' => 
        array (
          'in' => 'path',
          'value' => 1,
        ),
      ),
    ),
    'tags' => 
    array (
      0 => 'permission',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
  'master.permission.update' => 
  array (
    'uri' => 'api/master/permission/{permission}',
    'name' => 'master.permission.update',
    'prefix' => 'api/master',
    'folder' => 'master/permission',
    'action' => 'update',
    'method' => 'PUT',
    'parameters' => 
    array (
      'params' => 
      array (
        'permission' => 1,
      ),
      'infos' => 
      array (
        'permission' => 
        array (
          'in' => 'path',
          'value' => 1,
        ),
      ),
    ),
    'data' => 
    array (
    ),
    'tags' => 
    array (
      0 => 'permission',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
);