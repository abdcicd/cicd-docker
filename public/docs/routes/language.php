<?php 
 return array (
  'master.language.index' => 
  array (
    'uri' => 'api/master/language',
    'name' => 'master.language.index',
    'prefix' => 'api/master',
    'folder' => 'master/language',
    'action' => 'index',
    'method' => 'GET',
    'tags' => 
    array (
      0 => 'language',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
  'master.language.store' => 
  array (
    'uri' => 'api/master/language',
    'name' => 'master.language.store',
    'prefix' => 'api/master',
    'folder' => 'master/language',
    'action' => 'store',
    'method' => 'POST',
    'data' => 
    array (
      'name' => 'english',
      'code' => 'en',
      'is_active' => 11120,
    ),
    'tags' => 
    array (
      0 => 'language',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
  'master.language.show' => 
  array (
    'uri' => 'api/master/language/{language}',
    'name' => 'master.language.show',
    'prefix' => 'api/master',
    'folder' => 'master/language',
    'action' => 'show',
    'method' => 'GET',
    'parameters' => 
    array (
      'params' => 
      array (
        'language' => 1,
      ),
      'infos' => 
      array (
        'language' => 
        array (
          'in' => 'path',
          'value' => 1,
        ),
      ),
    ),
    'tags' => 
    array (
      0 => 'language',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
  'master.language.update' => 
  array (
    'uri' => 'api/master/language/{language}',
    'name' => 'master.language.update',
    'prefix' => 'api/master',
    'folder' => 'master/language',
    'action' => 'update',
    'method' => 'PUT',
    'parameters' => 
    array (
      'params' => 
      array (
        'language' => 1,
      ),
      'infos' => 
      array (
        'language' => 
        array (
          'in' => 'path',
          'value' => 1,
        ),
      ),
    ),
    'data' => 
    array (
      // 'name' => 1,
      // 'code' => 1,
      'is_active' => 1,
    ),
    'tags' => 
    array (
      0 => 'language',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
  'master.language.destroy' => 
  array (
    'uri' => 'api/master/language/{language}',
    'name' => 'master.language.destroy',
    'prefix' => 'api/master',
    'folder' => 'master/language',
    'action' => 'destroy',
    'method' => 'DELETE',
    'parameters' => 
    array (
      'params' => 
      array (
        'language' => 3,
      ),
      'infos' => 
      array (
        'language' => 
        array (
          'in' => 'path',
          'value' => 3,
        ),
      ),
    ),
    'tags' => 
    array (
      0 => 'language',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
);