<?php 
 return array (
  'master.company.index' => 
  array (
    'uri' => 'api/master/company',
    'name' => 'master.company.index',
    'prefix' => 'api/master',
    'folder' => 'master/company',
    'action' => 'index',
    'method' => 'GET',
    'tags' => 
    array (
      0 => 'company',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
  'master.company.store' => 
  array (
    'uri' => 'api/master/company',
    'name' => 'master.company.store',
    'prefix' => 'api/master',
    'folder' => 'master/company',
    'action' => 'store',
    'method' => 'POST',
    'data' => 
    array (
      'name' => 'tUJyUQsk1s',
      'code' => "edaskdKSa",
      'type' => 10
    ),
    'tags' => 
    array (
      0 => 'company',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
  'master.company.show' => 
  array (
    'uri' => 'api/master/company/{company}',
    'name' => 'master.company.show',
    'prefix' => 'api/master',
    'folder' => 'master/company',
    'action' => 'show',
    'method' => 'GET',
    'parameters' => 
    array (
      'params' => 
      array (
        'company' => 1,
      ),
      'infos' => 
      array (
        'company' => 
        array (
          'in' => 'path',
          'value' => 1,
        ),
      ),
    ),
    'tags' => 
    array (
      0 => 'company',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
  'master.company.update' => 
  array (
    'uri' => 'api/master/company/{company}',
    'name' => 'master.company.update',
    'prefix' => 'api/master',
    'folder' => 'master/company',
    'action' => 'update',
    'method' => 'PUT',
    'parameters' => 
    array (
      'params' => 
      array (
        'company' => 1,
      ),
      'infos' => 
      array (
        'company' => 
        array (
          'in' => 'path',
          'value' => 1,
        ),
      ),
    ),
    'data' => 
    array (
      'name' => '5jYRSHvDzL',
    ),
    'tags' => 
    array (
      0 => 'company',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
  'master.company.destroy' => 
  array (
    'uri' => 'api/master/company/{company}',
    'name' => 'master.company.destroy',
    'prefix' => 'api/master',
    'folder' => 'master/company',
    'action' => 'destroy',
    'method' => 'DELETE',
    'parameters' => 
    array (
      'params' => 
      array (
        'company' => 2,
      ),
      'infos' => 
      array (
        'company' => 
        array (
          'in' => 'path',
          'value' => 2,
        ),
      ),
    ),
    'tags' => 
    array (
      0 => 'company',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
);