<?php 
 return array (
  'master.action.index' => 
  array (
    'uri' => 'api/master/action',
    'name' => 'master.action.index',
    'prefix' => 'api/master',
    'folder' => 'master/action',
    'action' => 'index',
    'method' => 'GET',
    'tags' => 
    array (
      0 => 'action',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
);