<?php

use Faker\Provider\File;

 return array (
  'master.container.index' => 
  array (
    'uri' => 'api/master/container',
    'name' => 'master.container.index',
    'prefix' => 'api/master',
    'folder' => 'master/container',
    'action' => 'index',
    'method' => 'GET',
    'tags' => 
    array (
      0 => 'container',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
  'master.container.show' => 
  array (
    'uri' => 'api/master/container/{container}',
    'name' => 'master.container.show',
    'prefix' => 'api/master',
    'folder' => 'master/container',
    'action' => 'show',
    'method' => 'GET',
    'parameters' => 
    array (
      'params' => 
      array (
        'container' => 1,
      ),
      'infos' => 
      array (
        'container' => 
        array (
          'in' => 'path',
          'value' => 1,
        ),
      ),
    ),
    'tags' => 
    array (
      0 => 'container',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
  'master.container.update' => 
  array (
    'uri' => 'api/master/container/{container}',
    'name' => 'master.container.update',
    'prefix' => 'api/master',
    'folder' => 'master/container',
    'action' => 'update',
    'method' => 'PUT',
    'parameters' => 
    array (
      'params' => 
      array (
        'container' => 2521,
      ),
      'infos' => 
      array (
        'container' => 
        array (
          'in' => 'path',
          'value' => 2521,
        ),
      ),
    ),
    'data' => 
    array (
      'invoice_number' => 'Kp98TlbBpZ',
      'bl_number' => 'qHEl7QToh1',
      'eta' => '2023-10-10',
      'ata' => '2023-10-10',
    ),
    'tags' => 
    array (
      0 => 'container',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
  'master.container.destroy' => 
  array (
    'uri' => 'api/master/container/{container}',
    'name' => 'master.container.destroy',
    'prefix' => 'api/master',
    'folder' => 'master/container',
    'action' => 'destroy',
    'method' => 'DELETE',
    'parameters' => 
    array (
      'params' => 
      array (
        'container' => 2521,
      ),
      'infos' => 
      array (
        'container' => 
        array (
          'in' => 'path',
          'value' => 2521,
        ),
      ),
    ),
    'tags' => 
    array (
      0 => 'container',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
  'master.container-move' => 
  array (
    'uri' => 'api/master/container-move/{container}',
    'name' => 'master.container-move',
    'prefix' => 'api/master',
    'folder' => 'master/container',
    'action' => 'move',
    'method' => 'PUT',
    'parameters' => 
    array (
      'params' => 
      array (
        'container' => 'A.1.1',
      ),
      'infos' => 
      array (
        'container' => 
        array (
          'in' => 'path',
          'value' => 'A.1.1',
        ),
      ),
    ),
    'data' => 
    array (
      'grid_name' => 'C.36.10',
    ),
    'tags' => 
    array (
      0 => 'container',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
  'master.container-output' => 
  array (
    'uri' => 'api/master/container-output/{container}',
    'name' => 'master.container-output',
    'prefix' => 'api/master',
    'folder' => 'master/container',
    'action' => 'output',
    'method' => 'DELETE',
    'parameters' => 
    array (
      'params' => 
      array (
        'container' => 'A.1.1',
      ),
      'infos' => 
      array (
        'container' => 
        array (
          'in' => 'path',
          'value' => 'A.1.1',
        ),
      ),
    ),
    'tags' => 
    array (
      0 => 'container',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
  'master.container-out' => 
  array (
    'uri' => 'api/master/container-out',
    'name' => 'master.container-out',
    'prefix' => 'api/master',
    'folder' => 'master/container',
    'action' => 'outList',
    'method' => 'GET',
    'parameters' => 
    array (
      'params' => 
      array (
      ),
      'infos' => 
      array (
      ),
    ),
    'tags' => 
    array (
      0 => 'container',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
  'master.container-matrix' => 
  array (
    'uri' => 'api/master/container-matrix',
    'name' => 'master.container-matrix',
    'prefix' => 'api/master',
    'folder' => 'master/container',
    'action' => 'matrix',
    'method' => 'GET',
    'parameters' => 
    array (
      'params' => 
      array (
      ),
      'infos' => 
      array (
      ),
    ),
    'tags' => 
    array (
      0 => 'container',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
  'master.container.store' => 
  array (
    'uri' => 'api/master/container',
    'name' => 'master.container.store',
    'prefix' => 'api/master',
    'folder' => 'master/container',
    'action' => 'store',
    'method' => 'POST',
    'data' => 
    array (
      'number' => '2521-container',
      'type' => 10,
      'status' => 10,
      'invoice_number' => 'Kp98TlbBpZ',
      'bl_number' => 'qHEl7QToh1',
      'shipping_date' => '2023-10-10',
      'eta' => '2023-10-10',
      'ata' => '2023-10-10',
      'shipper_id' => 1,
      'forwarder_id' => 1,
      'consignee_id' => 1,
    ),
    'tags' => 
    array (
      0 => 'container',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
  'master.container-input' => 
  array (
    'uri' => 'api/master/container-input',
    'name' => 'master.container-input',
    'prefix' => 'api/master',
    'folder' => 'master/container',
    'action' => 'input',
    'method' => 'POST',
    'data' => 
    array (
      'grid_name' => 'A.1.1',
      'container_id' => NULL,
      'status' => 10,
      'number' => '2521-container',
    ),
    'tags' => 
    array (
      0 => 'container',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
  'master.container-file' => 
  array (
    'uri' => 'api/master/container-file',
    'name' => 'master.container-file',
    'prefix' => 'api/master',
    'folder' => 'master/container',
    'action' => 'uploadData',
    'method' => 'POST',
    'data' => 
    array (
      'file' => 1,
    ),
    'tags' => 
    array (
      0 => 'container',
    ),
    'description' => '',
    'content-type' => 'multipart/form-data',
    'auth' => true,
  ),
);