<?php 
 return array (
  'master.area.index' => 
  array (
    'uri' => 'api/master/area',
    'name' => 'master.area.index',
    'prefix' => 'api/master',
    'folder' => 'master/area',
    'action' => 'index',
    'method' => 'GET',
    'tags' => 
    array (
      0 => 'area',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
  'master.area.store' => 
  array (
    'uri' => 'api/master/area',
    'name' => 'master.area.store',
    'prefix' => 'api/master',
    'folder' => 'master/area',
    'action' => 'store',
    'method' => 'POST',
    'data' => 
    array (
      'name' => 'C',
      'x' => 100,
      'y' => 6,
      'limit' => 4,
      'description' => 'EXYsZDPbP6',
    ),
    'tags' => 
    array (
      0 => 'area',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
  'master.area.show' => 
  array (
    'uri' => 'api/master/area/{area}',
    'name' => 'master.area.show',
    'prefix' => 'api/master',
    'folder' => 'master/area',
    'action' => 'show',
    'method' => 'GET',
    'parameters' => 
    array (
      'params' => 
      array (
        'area' => 1,
      ),
      'infos' => 
      array (
        'area' => 
        array (
          'in' => 'path',
          'value' => 1,
        ),
      ),
    ),
    'tags' => 
    array (
      0 => 'area',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
  'master.area.update' => 
  array (
    'uri' => 'api/master/area/{area}',
    'name' => 'master.area.update',
    'prefix' => 'api/master',
    'folder' => 'master/area',
    'action' => 'update',
    'method' => 'PUT',
    'parameters' => 
    array (
      'params' => 
      array (
        'area' => 1,
      ),
      'infos' => 
      array (
        'area' => 
        array (
          'in' => 'path',
          'value' => 1,
        ),
      ),
    ),
    'data' => 
    array (
      'name' => 'A',
      'limit' => 3,
      'x' => 200,
      'y' => 7,
      'buffer' => 0,
      'description' => 'asdcxw',
    ),
    'tags' => 
    array (
      0 => 'area',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
  'master.area.destroy' => 
  array (
    'uri' => 'api/master/area/{area}',
    'name' => 'master.area.destroy',
    'prefix' => 'api/master',
    'folder' => 'master/area',
    'action' => 'destroy',
    'method' => 'DELETE',
    'parameters' => 
    array (
      'params' => 
      array (
        'area' => 2,
      ),
      'infos' => 
      array (
        'area' => 
        array (
          'in' => 'path',
          'value' => 1,
        ),
      ),
    ),
    'tags' => 
    array (
      0 => 'area',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
);