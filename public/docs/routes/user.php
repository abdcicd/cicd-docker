<?php 
 return array (
  'master.user.index' => 
  array (
    'uri' => 'api/master/user',
    'name' => 'master.user.index',
    'prefix' => 'api/master',
    'folder' => 'master/user',
    'action' => 'index',
    'method' => 'GET',
    'parameters' => 
    array (
      'params' => 
      array (
      ),
      'infos' => 
      array (
      ),
    ),
    'tags' => 
    array (
      0 => 'user',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
  'master.user.store' => 
  array (
    'uri' => 'api/master/user',
    'name' => 'master.user.store',
    'prefix' => 'api/master',
    'folder' => 'master/user',
    'action' => 'store',
    'method' => 'POST',
    'parameters' => 
    array (
      'params' => 
      array (
      ),
      'infos' => 
      array (
      ),
    ),
    'data' => 
    array (
      'username' => 'ZB0513',
      'password' => 'Adm@0513',
      'firstname' => 'Abdumannon',
      'lastname' => 'Norboyev',
    ),
    'tags' => 
    array (
      0 => 'user',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
  'master.user.show' => 
  array (
    'uri' => 'api/master/user/{user}',
    'name' => 'master.user.show',
    'prefix' => 'api/master',
    'folder' => 'master/user',
    'action' => 'show',
    'method' => 'GET',
    'parameters' => 
    array (
      'params' => 
      array (
        'user' => '3810de26-e562-407c-9c4b-502d78d8f061',
      ),
      'infos' => 
      array (
        'user' => 
        array (
          'in' => 'path',
          'value' => '3810de26-e562-407c-9c4b-502d78d8f061',
        ),
      ),
    ),
    'tags' => 
    array (
      0 => 'user',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
  'master.user.update' => 
  array (
    'uri' => 'api/master/user/{user}',
    'name' => 'master.user.update',
    'prefix' => 'api/master',
    'folder' => 'master/user',
    'action' => 'update',
    'method' => 'PUT',
    'parameters' => 
    array (
      'params' => 
      array (
        'user' => '3810de26-e562-407c-9c4b-502d78d8f061',
      ),
      'infos' => 
      array (
        'user' => 
        array (
          'in' => 'path',
          'value' => '3810de26-e562-407c-9c4b-502d78d8f061',
        ),
      ),
    ),
    'data' => 
    array (
      'firstname' => 'Abdumannon',
      'lastname' => 'Norboyev',
    ),
    'tags' => 
    array (
      0 => 'user',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
  'master.user.destroy' => 
  array (
    'uri' => 'api/master/user/{user}',
    'name' => 'master.user.destroy',
    'prefix' => 'api/master',
    'folder' => 'master/user',
    'action' => 'destroy',
    'method' => 'DELETE',
    'parameters' => 
    array (
      'params' => 
      array (
        'user' => '3e225436-285a-4926-8050-0122739879c7',
      ),
      'infos' => 
      array (
        'user' => 
        array (
          'in' => 'path',
          'value' => '3e225436-285a-4926-8050-0122739879c7',
        ),
      ),
    ),
    'tags' => 
    array (
      0 => 'user',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
  'master.user-profile' => 
  array (
    'uri' => 'api/master/user-profile',
    'name' => 'master.user-profile',
    'prefix' => 'api/master',
    'folder' => 'master/user',
    'action' => 'profile',
    'method' => 'GET',
    'parameters' => 
    array (
      'params' => 
      array (
      ),
      'infos' => 
      array (
      ),
    ),
    'tags' => 
    array (
      0 => 'user',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
);