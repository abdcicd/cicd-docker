<?php 
 return array (
  'master.ad-users' => 
  array (
    'uri' => 'api/master/ad-users',
    'name' => 'master.ad-users',
    'prefix' => 'api/master',
    'folder' => 'master/auth',
    'action' => 'adUsers',
    'method' => 'GET',
    'parameters' => 
    array (
      'params' => 
      array (
      ),
      'infos' => 
      array (
      ),
    ),
    'tags' => 
    array (
      0 => 'auth',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
  'master.user-attach-role' => 
  array (
    'uri' => 'api/master/user-attach-role/{id}',
    'name' => 'master.user-attach-role',
    'prefix' => 'api/master',
    'folder' => 'master/auth',
    'action' => 'attachRole',
    'method' => 'PUT',
    'parameters' => 
    array (
      'params' => 
      array (
        'id' => 'b401fba4-949c-417f-add0-b4cabd63a6b3',
      ),
      'infos' => 
      array (
        'id' => 
        array (
          'in' => 'path',
          'value' => 'b401fba4-949c-417f-add0-b4cabd63a6b3',
        ),
      ),
    ),
    'data' => 
    array (
      'role_name' => 'superadmin',
    ),
    'tags' => 
    array (
      0 => 'auth',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
  'master.user-detach-role' => 
  array (
    'uri' => 'api/master/user-detach-role/{id}',
    'name' => 'master.user-detach-role',
    'prefix' => 'api/master',
    'folder' => 'master/auth',
    'action' => 'detachRole',
    'method' => 'PUT',
    'parameters' => 
    array (
      'params' => 
      array (
        'id' => 'b401fba4-949c-417f-add0-b4cabd63a6b3',
      ),
      'infos' => 
      array (
        'id' => 
        array (
          'in' => 'path',
          'value' => 'b401fba4-949c-417f-add0-b4cabd63a6b3',
        ),
      ),
    ),
    'data' => 
    array (
      'role_name' => 'superadmin',
    ),
    'tags' => 
    array (
      0 => 'auth',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
  'master.user-sync-roles' => 
  array (
    'uri' => 'api/master/user-sync-role/{id}',
    'name' => 'master.user-sync-roles',
    'prefix' => 'api/master',
    'folder' => 'master/auth',
    'action' => 'syncRoles',
    'method' => 'PUT',
    'parameters' => 
    array (
      'params' => 
      array (
        'id' => 'ddf4851a-7ef4-48c9-a82d-8fb1d12abb07',
      ),
      'infos' => 
      array (
        'id' => 
        array (
          'in' => 'path',
          'value' => 'ddf4851a-7ef4-48c9-a82d-8fb1d12abb07',
        ),
      ),
    ),
    'data' => 
    array (
      'roles' => 
      array (
        0 => 'superadmin',
      ),
    ),
    'tags' => 
    array (
      0 => 'auth',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
  'auth.login' => 
  array (
    'uri' => 'api/auth/login',
    'name' => 'auth.login',
    'prefix' => 'api/auth',
    'folder' => 'auth/auth',
    'action' => 'login',
    'method' => 'POST',
    'parameters' => 
    array (
      'params' => 
      array (
      ),
      'infos' => 
      array (
      ),
    ),
    'data' => 
    array (
      'grant_type' => 'password',
      'client_secret' => 'Mt57LfRyUwwWIuKfSXnNzQAeWxQY0JFNerkrLymd',
      'client_id' => 2,
      'username' => 'AN0657',
      'password' => 'Adm@0657',
    ),
    'tags' => 
    array (
      0 => 'auth',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
);