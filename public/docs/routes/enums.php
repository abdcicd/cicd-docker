<?php 
 return array (
  'master.list-container-status-types' => 
  array (
    'uri' => 'api/master/list-container-status-types',
    'name' => 'master.list-container-status-types',
    'prefix' => 'api/master',
    'folder' => 'master/enums',
    'action' => 'containerStatusTypes',
    'method' => 'GET',
    'parameters' => 
    array (
      'params' => 
      array (
      ),
      'infos' => 
      array (
      ),
    ),
    'tags' => 
    array (
      0 => 'enums',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
  'master.list-company-types' => 
  array (
    'uri' => 'api/master/list-company-types',
    'name' => 'master.list-company-types',
    'prefix' => 'api/master',
    'folder' => 'master/enums',
    'action' => 'companyTypes',
    'method' => 'GET',
    'parameters' => 
    array (
      'params' => 
      array (
      ),
      'infos' => 
      array (
      ),
    ),
    'tags' => 
    array (
      0 => 'enums',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
  'master.list-container-types' => 
  array (
    'uri' => 'api/master/list-container-types',
    'name' => 'master.list-container-types',
    'prefix' => 'api/master',
    'folder' => 'master/enums',
    'action' => 'containerTypes',
    'method' => 'GET',
    'parameters' => 
    array (
      'params' => 
      array (
      ),
      'infos' => 
      array (
      ),
    ),
    'tags' => 
    array (
      0 => 'enums',
    ),
    'description' => '',
    'content-type' => 'application/json',
    'auth' => true,
  ),
);