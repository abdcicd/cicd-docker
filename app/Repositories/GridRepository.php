<?php

namespace App\Repositories;

use App\Models\Grid;
use App\Http\Resources\GridResource;

class GridRepository extends BaseRepository
{
    public function __construct(Grid $modelInstance)
    {
        $this->model = $modelInstance;
        $this->resource = GridResource::class;

        $this->likableFields = [
            'name',
        ];

        $this->equalableFields = [
            'id',
            'area_id',
            'x_axis',
            'y_axis',
            'created_by',
            'updated_by',
        ];

        $this->id = 'name';

        parent::__construct();
    }

    public function getGridContainersCount($grid)
    {
        if(is_string($grid)) return $this->findById($grid)?->positions()->count();
        else if ($grid instanceof $this->model) return $grid?->positions()->count();
    }
    
    public function getGridAreaLimit($grid)
    {
        if(is_string($grid)) return $this->findById($grid)?->area?->limit;
        else if ($grid instanceof $this->model) return $grid?->area?->limit;
    }

    public function getLastGrid($area_id)
    {
        return $this->setQuery()->where('area_id', $area_id)->orderBy('id', 'desc')->first();
    }

    public function theTopPosition($name)
    {
        if ($grid = $this->findById($name)) {
            return $grid->positions()->orderBy('z_axis', 'desc')->first();
        }
    }

    public function positionExists($id)
    {
        $this->id = 'id';
        return $this->findById($id)?->positions()->exists();
    }
}
