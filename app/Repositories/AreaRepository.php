<?php

namespace App\Repositories;

use App\Models\Area;
use App\Http\Resources\AreaResource;
use App\Http\Resources\AreaResourceSpecial;

class AreaRepository extends BaseRepository
{
    public function __construct(Area $modelInstance)
    {
        $this->model = $modelInstance;
        $this->resource = AreaResource::class;
        $this->resources = [AreaResource::class, AreaResourceSpecial::class];

        $this->likableFields = [
            'name',
            'description'
        ];

        $this->equalableFields = [
            'id',
            'x',
            'y',
            'buffer',
            'created_by',
            'updated_by',
        ];

        parent::__construct();
    }

    public function getAreas()
    {
        $rel = [
            'grids' => [
                'id', 'name', 'area_id', 'x_axis', 'y_axis',
                'positions' => [
                    'grid_name', 'z_axis', 'container_id'
                ]
            ]
        ];
        return $this->setQuery()->select('id', 'limit', 'x', 'y', 'name')->orderBy('name', 'asc')->with(parseToRelation($rel))->get();
    }
}
