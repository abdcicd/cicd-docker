<?php

namespace App\Repositories;

use App\Models\Action;
use App\Http\Resources\ActionResource;

class ActionRepository extends BaseRepository
{
    public function __construct(Action $modelInstance)
    {
        $this->model = $modelInstance;
        $this->resource = ActionResource::class;

        $this->likableFields = [
            'container_number',
            'container_size',
            'shipper_name',
            'forwarder_name',
        ];

        $this->equalableFields = [
            'container_number',
            'container_size',
            'shipper_name',
            'forwarder_name',
            'created_at',
            'created_by',
        ];
        parent::__construct();
    }
}
