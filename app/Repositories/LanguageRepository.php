<?php


namespace App\Repositories;

use App\Http\Resources\LanguageResource;
use App\Models\Language;

class LanguageRepository extends BaseRepository
{
    public function __construct(Language $language)
    {
        $this->model = $language;
        $this->resource = LanguageResource::class;

        $this->likableFields = [
            'name',
            'code',
        ];

        $this->equalableFields = [
            'id',
            'is_active',
            'created_by',
            'updated_by',
            'deleted_by',
        ];

        parent::__construct();
    }
}
