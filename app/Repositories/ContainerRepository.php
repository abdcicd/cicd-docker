<?php

namespace App\Repositories;

use App\Http\Resources\ContainerOutResource;
use App\Models\Container;
use App\Http\Resources\ContainerResource;
use App\Http\Resources\ContainerShowResource;

class ContainerRepository extends BaseRepository
{
    public function __construct(Container $modelInstance)
    {
        $this->model = $modelInstance;
        $this->resource = ContainerResource::class;
        $this->showResource = ContainerShowResource::class;
        $this->resources = [ContainerResource::class, ContainerOutResource::class];

        $this->likableFields = [
            'number',
            'invoice_number',
            'bl_number',
        ];

        $this->equalableFields = [
            'id',
            'type',
            'status',
            'shipper_id',
            'forwarder_id',
            'consignee_id',
        ];

        $this->dateIntervalFields = [
            'shipping_date',
            'eta',
            'ata',
            'created_at',
            'created_by',
        ];

        parent::__construct();
    }


    public function specialFilter()
    {
        $this->query->where(function ($q) {
            if ($cell = request('cell')) {
                $q->whereHas('position', function ($q) use ($cell) {
                    $q->where('grid_name', 'ilike', "$cell%");
                });
            }
            if ($floors = request('floor')) {
                $q->whereHas('position', function ($q) use ($floors) {
                    $q->whereIn('z_axis', explode(',', $floors));
                });
            }
            if ($shipper = request('shipper')) {
                $q->whereHas('shipper', function ($q) use ($shipper) {
                    $q->whereIn('id', explode(',', $shipper));
                });
            }
            if ($forwarder = request('forwarder')) {
                $q->whereHas('forwarder', function ($q) use ($forwarder) {
                    $q->whereIn('id', explode(',', $forwarder));
                });
            }
            if ($consignee = request('consignee')) {
                $q->whereHas('consignee', function ($q) use ($consignee) {
                    $q->whereIn('id', explode(',', $consignee));
                });
            }
        });
    }
}
