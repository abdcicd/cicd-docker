<?php

namespace App\Repositories;

use App\Models\Role;
use App\Http\Resources\RoleResource;

class RoleRepository extends BaseRepository
{
    public function __construct(Role $modelInstance)
    {
        $this->model = $modelInstance;
        $this->resource = RoleResource::class;

        $this->relations = [
            'permissions.translation',
            'permissions.translations',
            'translation',
            'translations',
        ];

        $this->likableFields = [
            'name',
        ];

        $this->equalableFields = [
            'id',
            'type',
            'organization_id',
        ];

        $this->translationFields = [
            'translation',
            'description',
        ];

        $this->defaultOrder = [['column' => 'id', 'direction' => 'asc']];

        parent::__construct();
    }
}
