<?php

namespace App\Repositories;

use App\Models\Permission;
use App\Http\Resources\PermissionResource;

class PermissionRepository extends BaseRepository
{
    public function __construct(Permission $modelInstance)
    {
        $this->model = $modelInstance;
        $this->resource = PermissionResource::class;

        $this->relations = [
            'translation',
            'translations',
        ];
        
        $this->likableFields = [
            'name',
        ];
        
        $this->equalableFields = [
            'id',
            'type',
        ];
        
        $this->translationFields = [
            'translation',
            'description',
        ];

        parent::__construct();
    }
}
