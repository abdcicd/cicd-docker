<?php

namespace App\Repositories;

use App\Models\Company;
use App\Http\Resources\CompanyResource;

class CompanyRepository extends BaseRepository
{
    public function __construct(Company $modelInstance)
    {
        $this->model = $modelInstance;
        $this->resource = CompanyResource::class;

        $this->likableFields = [
            'name',
        ];

        $this->equalableFields = [
            'id',
            'type',
            'code',
            'created_by',
            'updated_by',
        ];

        parent::__construct();
    }

    public function companiesWithCode()
    {
        $companies = [];
        foreach ($this->setQuery()->get() as $company) {
            $companies[$company->code] = $company->id;
        }
        return $companies;
    }
}
