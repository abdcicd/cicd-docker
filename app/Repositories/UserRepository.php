<?php

namespace App\Repositories;

use Adldap\Laravel\Facades\Adldap;
use Illuminate\Support\Str;
use App\Http\Resources\AdUserResource;
use App\Http\Resources\UserResource;
use App\Models\User;

class UserRepository extends BaseRepository
{
    public function __construct(
        protected User $modelInstance

    ) {
        $this->model = $modelInstance;
        $this->manyRelation = 'roles';
        $this->resource = UserResource::class;

        $this->likableFields = [
            'username',
            'firstname',
            'lastname'
        ];

        $this->equalableFields = [
            'uuid',
        ];

        $this->dateIntervalFields = [
            'created_at'
        ];

        $this->id = 'uuid';

        parent::__construct();
    }

    public function editUserInLogin($data)
    {
        $this->id = 'username';
        $this->editWithThrow($data['username'], $data);
    }

    public function users()
    {
        return $this->setQuery()->select('username')->get()->pluck('username')->toArray();
    }

    public function adUsers()
    {
        $users = $this->users();
        $query = Adldap::search()->users();
        if (request('username')) {
            $query->whereIn('samaccountname', explode(',', request('username')));
        }
        $data = [];
        $adUsers = $query->select('samaccountname', 'givenname', 'sn')->get();
        foreach ($adUsers as $adUser) {
            $username = is_array($adUser->samaccountname) ? $adUser->samaccountname[0] : $adUser->samaccountname;
            if (in_array($username, $users)) continue;
            $firstname = is_array($adUser->givenname) ? $adUser->givenname[0] : $adUser->givenname;
            $lastname = is_array($adUser->sn) ? $adUser->sn[0] : $adUser->sn;
            if ($search = request('search')) {
                if (Str::startsWith(Str::lower($username), Str::lower($search))) {
                    $data[] = compact('username', 'firstname', 'lastname');
                } else continue;
            } else $data[] = compact('username', 'firstname', 'lastname');
        }
        return $data;
    }

    public function usernameExists($username)
    {
        return empty(Adldap::search()->users()->where('samaccountname', '=', $username)->get()->toArray());
    }
}
