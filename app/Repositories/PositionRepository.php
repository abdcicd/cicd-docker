<?php

namespace App\Repositories;

use App\Models\Position;
use App\Http\Resources\PositionResource;

class PositionRepository extends BaseRepository
{
    public function __construct(Position $modelInstance)
    {
        $this->model = $modelInstance;
        $this->resource = PositionResource::class;

        $this->likableFields = [
        ];
        
        $this->equalableFields = [
            'container_id',
            'id',
            'grid_name',
            'z_axis',
            'created_at',
            'created_by',
            'updated_by',
        ];

        parent::__construct();
    }

    public function containerExists($containerId)
    {
        return $this->existsByColumn('container_id', $containerId);
    }
}
