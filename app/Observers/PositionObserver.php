<?php

namespace App\Observers;

use App\Enums\ContainerStatusEnum;
use App\Enums\ContainerTypeEnum;
use App\Models\Action;
use App\Models\Position;

class PositionObserver
{
    public function created(Position $position)
    {
        $this->change($position);
    }

    public function updated(Position $position)
    {
        $this->change($position);
    }

    public function deleted(Position $position)
    {
        $this->change($position);
    }

    protected function change($position)
    {
        $old_action = Action::where('container_id', $position?->container_id)->orderBy('created_at', 'DESC')->first();
        $ps = $position?->grid?->name . "." . $position->z_axis;
        $isLeave = false;
        if ($old_action && $old_action->position == $ps) {
            $isLeave = true;
        }
        Action::create([
            'container_id' => $position?->container_id,
            'container_number' => $position?->container?->number,
            'container_type' => ContainerTypeEnum::getLabel($position?->container?->type),
            'container_status' => ContainerStatusEnum::getLabel($position?->container?->status),
            'shipper_name' => $position?->container?->shipper?->name,
            'forwarder_name' => $position?->container?->forwarder?->name,
            'consignee_name' => $position?->container?->consignee?->name,
            'user_name' => auth()->user() ? auth()->user()?->firstname . " " . auth()->user()?->lastname : "username",
            'action' => $isLeave ? $ps . " dan olib chiqildi" : ($old_action && $old_action->position != "Maydondan olindi" ?  $old_action->position . " dan " . $ps . " ga qo'yildi"  : $ps . " ga qo'yildi"),
            'position' => $isLeave ? "Maydondan olindi" : $ps,
            'old_position' => $old_action ? $old_action->position : null,
        ]);
        try {
        } catch (\Throwable $th) {
            dd($th->getMessage(), $position->container->toArray());
        }
    }
}
