<?php

namespace App\Observers;

use App\Models\Container;

class ContainerObserver
{
    public function created(Container $container)
    {
    }

    public function updated(Container $container)
    {

    }
}
