<?php

namespace App\Rules;

use App\Models\Grid;
use Illuminate\Contracts\Validation\Rule;

class GridExists implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public $column;

    public function __construct($column = 'id')
    {
        $this->column = $column;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return !(Grid::where($this->column, $value)->first() == null);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Grid is not found';
    }
}
