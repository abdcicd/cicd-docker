<?php

namespace App\Rules;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Contracts\Validation\Rule;

class RoleExists implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return !(Role::where('name', $value)->first() == null);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Role is not found';
    }
}
