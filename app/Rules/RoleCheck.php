<?php

namespace App\Rules;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Contracts\Validation\Rule;

class RoleCheck implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // if (!auth()->user()->hasRole('superadmin')) {
        //     if ($value == 'superadmin' || $value == 'company-admin') {
        //         return false;
        //     }
        // }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'This company can\'t be passed';
    }
}
