<?php

namespace App\Console\Commands;

use Abd\Docmaker\Console\GenDoc as GenerateDocument;

class GenDoc extends GenerateDocument
{
    public function handle()
    {
        $this->mainDocFile = 'main';
        $this->prefixes = [
            'api/master',
            'api/auth'
        ];
        $this->authData = [
            "grant_type" => "password",
            "client_secret" => "Mt57LfRyUwwWIuKfSXnNzQAeWxQY0JFNerkrLymd",
            "client_id" => 2,
            "username" => "AN0657",
            "password" => "Adm@0657"
        ];
        parent::handle();
    }
}
