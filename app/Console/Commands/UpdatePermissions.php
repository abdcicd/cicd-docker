<?php

namespace App\Console\Commands;

use App\Helpers\CommonHelper;
use App\Models\Language;
use App\Models\Permission;
use App\Models\PermissionTranslation;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

class UpdatePermissions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update-permissions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update permissions according to the routes';

    /**
     * Permission service instance.
     *
     * @var string
     */
    protected $permissionService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        DB::connection()->beginTransaction();

        try {

            // active languages
            $languages = Language::where('is_active', true)->get();

            $count = 0;
            $actionPermissions = [];
            // dd(Route::getRoutes());
            foreach (Route::getRoutes() as $route) {

                if (!str_contains($route->getName(), 'master')) continue;
                
                // route Controller action dan dan permission yasab olamiz
                $actionPermission =  CommonHelper::actionPermission($route);

                // permission va transaltions yoziladi
                // agar permission oldin yozilgan bo'lsa qayta yozmaymiz
                $exists = Permission::firstWhere('name', $actionPermission['permission']);
                if (!$exists) {
                    $count++;
                    $permission = Permission::create([
                        'name' => $actionPermission['permission'],
                        'type' => $actionPermission['permissionType'],
                    ]);

                    foreach ($languages as $language) {
                        PermissionTranslation::create([
                            'object_id' => $permission->id,
                            'translation' => $actionPermission['permission'],
                            'description' => $actionPermission['permission'],
                            'language_code' => $language->code,
                        ]);
                    }
                }

                $actionPermissions[] = $actionPermission['permission'];
            }

            // agar permissionlar action permissionlarida bo'lmasa db dan o'chiramiz
            // bu holat action o'chib ketganda sodir bo'ladi
            $permissions = Permission::all();
            $countDeleted = 0;
            foreach($permissions as $permission){
                if(!in_array($permission->name, $actionPermissions)){
                    $permission->delete();
                    $countDeleted++;
                }
            }

            // update cache
            Cache::forget('permissions');
            Cache::put('permissions', Permission::get());


            DB::connection()->commit();

            $this->info('<fg=green>' . $count . ' permission(s) created!</>');
            $this->info('<fg=green>' . $countDeleted . ' permission(s) deleted!</>');

        } catch (\Throwable $th) {
            DB::connection()->rollBack();
            throw $th;
        }
        
    }
}
