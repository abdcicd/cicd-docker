<?php

namespace App\Services;

use App\Repositories\ActionRepository;

class ActionService extends BaseService
{
    public function __construct(
        protected ActionRepository $actionRepository
    ) {
    }
}
