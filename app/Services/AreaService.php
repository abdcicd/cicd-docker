<?php

namespace App\Services;

use App\Repositories\AreaRepository;

class AreaService extends BaseService
{
    public function __construct(
        protected AreaRepository $areaRepository
    ) {
    }
}
