<?php

namespace App\Services;

use App\Repositories\PermissionRepository;

class PermissionService extends BaseService
{
    public function __construct(
        protected PermissionRepository $permissionRepository
    ) {
    }

    public function getList($data)
    {
        return $this->permissionRepository->getList($data);
    }

    public function show($id)
    {
        return $this->permissionRepository->show($id);
    }

    public function edit($id, $data)
    {
        return $this->permissionRepository->edit($id, $data);
    }
}
