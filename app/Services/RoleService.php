<?php

namespace App\Services;

use App\Repositories\RoleRepository;

class RoleService extends BaseService
{
    public function __construct(
        protected RoleRepository $roleRepository
    ) {
    }

    public function getList($data)
    {
        $this->roleRepository->with(['translations', 'permissions', 'users' => ['id', 'uuid', 'username']]);
        return $this->roleRepository->getList($data);
    }

    public function show($id)
    {
        $this->roleRepository->with(['translations', 'permissions', 'users' => ['id', 'uuid', 'username']]);
        return $this->roleRepository->show($id);
    }

    public function syncPermissions($id, $data)
    {
        $role = $this->roleRepository->findById($id);
        if ($role) {
            $role->permissions()->sync($data['permissions']);
            $this->setResponseData(data: $this->roleRepository->toResource($role));
        } else
            $this->setResponseData(status: 0, message: 'Not found', code: 404);
        return $this->response();
    }
}
