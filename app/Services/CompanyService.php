<?php

namespace App\Services;

use App\Repositories\CompanyRepository;

class CompanyService extends BaseService
{
    public function __construct(
        protected CompanyRepository $companyRepository
    ) {
    }

    public function getList($data)
    {
        $this->companyRepository->willParseToRelation = [
            'admin' => ['id', 'uuid', 'username']
        ];
        return $this->companyRepository->getList($data);
    }

    public function show($id)
    {
        $this->companyRepository->willParseToRelation = [
            'admin' => ['id', 'uuid', 'username']
        ];
        return $this->companyRepository->show($id);
    }
}
