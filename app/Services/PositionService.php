<?php

namespace App\Services;

use App\Repositories\AreaRepository;
use App\Repositories\ContainerRepository;
use App\Repositories\GridRepository;
use App\Repositories\PositionRepository;

class PositionService extends BaseService
{
    public function __construct(
        protected PositionRepository $positionRepository,
        protected GridRepository $gridRepository,
        protected ContainerRepository $containerRepository
    ) {
    }

    public function input($data)
    {
        $grid = $this->gridRepository->findById($data['grid_name']);
        $count = $this->gridRepository->getGridContainersCount($grid);
        $limit = $this->gridRepository->getGridAreaLimit($grid);

        $data = $this->optional($data);

        if ($limit == $count) {
            $count++;
            return $this->makeResponse(status: 0, message: "Max limit $limit for $grid->name grid. This is $count-th container", code: 400);
        }

        if (!$this->containerRepository->exists($data('container_id')) && !$data('number')) {
            return $this->makeResponse(status: 0, message: 'Container not found', code: 404);
        }

        if ($this->positionRepository->existsByColumn('container_id', $data('container_id')) && !$data('number')) {
            return $this->makeResponse(status: 0, message: 'This container already exist in area', code: 400);
        }

        $this->withTransaction(
            executer: function () use ($data, $count) {
                $container_id = $data('container_id');
                if ($data('number')) {
                    $newContainer = $this->containerRepository->createWithThrow([
                        'number' => $data('number'),
                        'status' => $data('status')
                    ]);
                    $container_id = $newContainer?->id;
                }
                $this->positionRepository->createWithThrow([
                    'grid_name' => $data('grid_name'),
                    'z_axis' => $count + 1,
                    'container_id' => $container_id
                ]);
                $this->setResponseData(status: 0, message: "Container input successfully !!!");
            }, catch: function ($error, $code) {
                $this->setResponseData(status: 0, message: $error, code: 500);
            }
        );

        return $this->response();
    }

    public function move($conOrGridname, $data)
    {
        if (!$position = $this->gridRepository->theTopPosition($conOrGridname)) {
            if ($position = $this->positionRepository->findBy('container_id', (int)$conOrGridname)) {
                if ($position->grid_name == $data['grid_name']) {
                    return $this->makeResponse(status: 0, message: "This container already had been set up this location", code: 400);
                }
            } else return $this->makeResponse(status: 0, message: "Container not found in area", code: 404);
            $z = $this->gridRepository->getGridContainersCount($position->grid);
            if ($position->z_axis != $z) {
                $diff = $z - $position->z_axis;
                $m = $diff > 1 ? 'are' : 'is';
                return $this->makeResponse(status: 0, message: "There $m $diff another container on the top", code: 400);
            }
        }
        $grid = $this->gridRepository->findById($data['grid_name']);
        $count = $this->gridRepository->getGridContainersCount($grid);
        $limit = $this->gridRepository->getGridAreaLimit($grid);
        if ($limit == $count) {
            $count++;
            return $this->makeResponse(status: 0, message: "Max limit $limit for $grid->name grid. This is $count-th container", code: 400);
        }
        $data['z_axis'] = $count + 1;
        return $this->positionRepository->edit($position?->id, $data);
    }

    public function output($conOrGridname)
    {
        if (!$position = $this->gridRepository->theTopPosition($conOrGridname)) {
            if (!$position = $this->positionRepository->findBy('container_id', (int)$conOrGridname)) {
                return $this->makeResponse(status: 0, message: "Container not found in area", code: 404);
            }
            $z = $this->gridRepository->getGridContainersCount($position->grid);
            if ($position->z_axis != $z) {
                $diff = $z - $position->z_axis;
                $m = $diff > 1 ? 'are' : 'is';
                return $this->makeResponse(status: 0, message: "There $m $diff another container on the top", code: 400);
            }
        }
        return $this->positionRepository->delete($position?->id);
    }


    protected function optional($array)
    {
        return fn ($key) => isset($array[$key]) ? $array[$key] : null;
    }
}
