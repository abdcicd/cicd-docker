<?php

namespace App\Services;

use App\Enums\CompanyTypeEnum;
use App\Enums\ContainerStatusEnum;
use App\Enums\ContainerTypeEnum;
use App\Enums\PermissionTypeEnum;

class EnumsService extends BaseService
{
    public function makeAction($enum)
    {
        $data = [];
        foreach ($enum::list(true) as $key => $value) {
            $data[] = [
                'code' => $key,
                'name' => $value,
            ];
        }
        return $this->makeResponse(data: $data);
    }


    public function containerTypes()
    {
        return $this->makeAction(ContainerTypeEnum::class);
    
    }
    public function containerStatusTypes()
    {
        return $this->makeAction(ContainerStatusEnum::class);
    }
    
    public function permissionTypes()
    {
        return $this->makeAction(PermissionTypeEnum::class);
    }

    public function companyTypes()
    {
        return $this->makeAction(CompanyTypeEnum::class);
    }
}