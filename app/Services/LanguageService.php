<?php

namespace App\Services;

use App\Repositories\LanguageRepository;

class LanguageService extends BaseService
{
    public function __construct(
        protected LanguageRepository $languageRepository
    ) {
    }

    public function list($data)
    {
        return $this->languageRepository->changeResource(1)->getList($data);
    }
}
