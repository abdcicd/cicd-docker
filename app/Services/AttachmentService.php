<?php

namespace App\Services;

use App\Models\Attachment;
use App\Services\BaseService;
use FFMpeg\Coordinate\TimeCode;
use FFMpeg\FFMpeg;
use FFMpeg\FFProbe;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Image;

class AttachmentService extends BaseService
{
    public $types = [
        'image' => [
            'extensions' => ['jpg', 'jpeg', 'png', 'gif'],
            'mime_types' => ['image/jpeg', 'image/png', 'image/gif'],
        ],
        'video' => [
            'extensions' => ['mp4', 'mov', 'avi', 'wmv', 'flv', 'mpg', 'mpeg'],
            'mime_types' => ['video/mp4', 'video/quicktime', 'video/x-ms-wmv', 'video/x-flv', 'video/mpeg', 'video/x-msvideo'],
        ],
        'audio' => [
            'extensions' => ['mp3', 'wav', 'ogg', 'aac', 'flac', 'wma', 'm4a'],
            'mime_types' => ['audio/mpeg', 'audio/x-wav', 'audio/ogg', 'audio/x-aac', 'audio/flac', 'audio/x-ms-wma'],
        ],
        'file' => [
            'extensions' => ['doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx', 'pdf', 'txt', 'zip', 'rar'],
            'mime_types' => ['application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/vnd.ms-powerpoint', 'application/vnd.openxmlformats-officedocument.presentationml.presentation', 'application/pdf', 'text/plain', 'application/zip', 'application/x-rar-compressed'],
        ],
    ];

    public function upload($file, $path = '', $dimensions = null)
    {
        $modelPath = Attachment::pathAddition() . $path;
        if ($file) {
            $filename = (string)Str::uuid();
            $extension = $file->getClientOriginalExtension();
            $name = $filename . '.' . $extension;
            $type = $this->getTypeByExtension($extension);

            if ($type == 'image') {
                $mini = Image::make($file)->resize($dimensions['width'] ?? 200, $dimensions['height'] ?? 200,
                    function ($constraint) {
                        $constraint->aspectRatio();
                    }
                );
                Storage::put($modelPath . '/mini/' . $name, $mini->encode());

                $middle = Image::make($file)->resize(600, 600, function ($constraint) {
                    $constraint->aspectRatio();
                });
                Storage::put($modelPath . '/middle/' . $name, $middle->encode());
            }

            if ($type == 'audio' or $type == 'video') {

                // getting duration of video and audio
                $ffprobe = FFProbe::create([
                    'ffmpeg.binaries' => exec('which ffmpeg'),
                    'ffprobe.binaries' => exec('which ffprobe')
                ]);
                $duration = $ffprobe->format($file)->get('duration');
            }

            if ($type == 'video') {

                // saving video cover
                $ffmpeg = FFMpeg::create([
                    'ffmpeg.binaries' => exec('which ffmpeg'),
                    'ffprobe.binaries' => exec('which ffprobe')
                ]);
                $video = $ffmpeg->open($file);
                $seconds = intval($duration / 2);
                $frame = $video->frame(TimeCode::fromSeconds($seconds));
                $coverFolder = storage_path('app/' . $modelPath . '/cover');
                if (!file_exists($coverFolder)) {
                    mkdir($coverFolder, 0777, true);
                }
                $frame->save($coverFolder . '/' . $filename . '.jpg');

                // getting video dimensions
                $videoDimensions = $ffprobe
                    ->streams($file)
                    ->videos()
                    ->first()
                    ->getDimensions();
                $videoWidth = $videoDimensions->getWidth();
                $videoHeight = $videoDimensions->getHeight();
            }

            if (Storage::putFileAs($modelPath, $file, $name)) {
                $info = [
                    'filename' => $filename,
                    'originalName' => pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME),
                    'extension' => $extension,
                    'folder' => $path,
                    'size' => $file->getSize(),
                    'type' => $type,
                ];

                if ($type == 'image') {
                    $orgDimensions = getimagesize($file);
                    $info['original_width'] = $orgDimensions[0];
                    $info['original_height'] = $orgDimensions[1];
                    $info['middle_width'] = $middle->width();
                    $info['middle_height'] = $middle->height();
                    $info['mini_width'] = $mini->width();
                    $info['mini_height'] = $mini->height();
                }

                if ($type == 'audio' or $type == 'video') {
                    $info['duration'] = (float)$duration;
                }

                if ($type == 'video') {
                    $info['original_width'] = $videoWidth;
                    $info['original_height'] = $videoHeight;
                }

                return $info;
            }
        }

        return false;
    }

    public function download($id, $size = '')
    {
        $file = Attachment::find($id);
        if ($file) {
            $path = (empty($size)) ? $file->fullPath : (($size == 'middle') ? $file->fullPathMiddle : $file->fullPathMini);
            if (Storage::exists($path)) {
                return Storage::download($path);
            } else {
                return response()->json(['message' => 'File not exists'], 404);
            }
        } else {
            return response()->json(['message' => 'File not found'], 404);
        }

        return false;
    }

    public function getTypeByExtension($ext)
    {
        foreach ($this->types as $type => $value) {
            if (in_array($ext, $value['extensions'])) {
                return $type;
            }
        }

        return 'other';
    }

    public function remove($id)
    {
        $file = Attachment::find($id);
        if ($file) {

            // removing file
            if (Storage::exists($file->fullPath)) {
                Storage::delete($file->fullPath);
            }

            // removing file middle
            if (Storage::exists($file->fullPathMiddle)) {
                Storage::delete($file->fullPathMiddle);
            }

            // removing file mini
            if (Storage::exists($file->fullPathMini)) {
                Storage::delete($file->fullPathMini);
            }

            // deleting file record from database
            $file->delete();

            $this->setReturnData(code: 204);

        } else {
            $this->setReturnData(status: 0, message: 'File not found', code: 404);
        }

        return $this->return();

    }
    public function update($id)
    {
        $file = Attachment::find($id);
        if ($file) {

            // removing file
            if (Storage::exists($file->fullPath)) {
                Storage::delete($file->fullPath);
            }

            // removing file middle
            if (Storage::exists($file->fullPathMiddle)) {
                Storage::delete($file->fullPathMiddle);
            }

            // removing file mini
            if (Storage::exists($file->fullPathMini)) {
                Storage::delete($file->fullPathMini);
            }

        } else {
        }
    }

}
