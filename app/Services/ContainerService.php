<?php

namespace App\Services;

use App\Repositories\AreaRepository;
use App\Repositories\CompanyRepository;
use App\Repositories\ContainerRepository;
use App\Repositories\PositionRepository;
use Error;
use OpenSpout\Reader\XLSX\Reader;

class ContainerService extends BaseService
{
    public function __construct(
        protected ContainerRepository $containerRepository,
        protected AreaRepository $areaRepository,
        protected PositionRepository $positionRepository,
        protected CompanyRepository $companyRepository
    ) {
    }

    public function matrix()
    {
        $areas = $this->areaRepository->getAreas();
        $matrix = [];
        foreach ($areas as $area) {
            $area_data = [];
            for ($j = 1; $j <= $area->y; $j++) {
                $area_data[$j - 1] = [];
            }
            foreach ($area->grids as $grid) {
                $grid_data = [];
                $grid_data['name'] = $grid->name;
                $containers = [];
                foreach ($grid->positions as $position) {
                    $containers[] = [
                        'z_axis' => $position->z_axis,
                        'container_id' => $position->container_id
                    ];
                }
                $grid_data['containers'] = $containers;
                $area_data[$grid->y_axis - 1][] = $grid_data;
            }
            $matrix[$area->name] = $area_data;
        }
        return $matrix;
    }

    public function show($connumber)
    {
        $this->containerRepository->willParseToRelation = [
            'position' => ['id', 'grid_name', 'z_axis', 'container_id'],
            'actions' => [fn ($q) => $q->orderBy('created_at', 'desc')->limit(5)],
            'shipper' => ['id', 'name', 'code'],
            'forwarder' => ['id', 'name', 'code'],
            'consignee' => ['id', 'name', 'code'],
        ];
        return $this->containerRepository->show($connumber);
    }

    public function outList($data)
    {
        $this->containerRepository->willParseToRelation = [
            'shipper' => ['id', 'name', 'code'],
            'forwarder' => ['id', 'name', 'code'],
            'consignee' => ['id', 'name', 'code'],
        ];
        $this->containerRepository->queryClosure = fn ($q) => $q->doesntHave('position');
        return $this->containerRepository->changeResource(1)->getList($data);
    }

    public function getList($data)
    {
        $this->containerRepository->willParseToRelation = [
            'position' => ['id', 'grid_name', 'z_axis', 'container_id'],
            'actions' => [fn ($q) => $q->orderBy('created_at', 'desc')->limit(5)],
            'shipper' => ['id', 'name', 'code'],
            'forwarder' => ['id', 'name', 'code'],
            'consignee' => ['id', 'name', 'code'],
        ];
        $this->containerRepository->queryClosure = function ($q) {
            $q->has('position');
            
            if ($sort = request('sort')) {
                $sortArray = explode(',', $sort);
                foreach ($sortArray as $s) {
                    $desc = str_starts_with($s, '-');
                    $type = $desc ? 'DESC' : 'ASC';
                    $field = $desc ? substr($s, 1) : $s;
                    switch ($field) {
                        case 'cell':
                            $q->leftJoin('positions AS p', 'p.container_id', 'containers.id');
                            $q->orderBy('p.grid_name', $type);
                            break;
                        case 'floor':
                            $q->leftJoin('positions AS p', 'p.container_id', 'containers.id');
                            $q->orderBy('p.z_axis', $type);
                            break;
                        case 'shipper':
                            $q->leftJoin('companies AS c', 'c.id', 'containers.shipper_id');
                            $q->orderBy('c.name', $type);
                            break;
                        case 'forwarder':
                            $q->leftJoin('companies AS c', 'c.id', 'containers.forwarder_id');
                            $q->orderBy('c.name', $type);
                            break;
                        case 'consignee':
                            $q->leftJoin('companies AS c', 'c.id', 'containers.consignee_id');
                            $q->orderBy('c.name', $type);
                            break;
                    }
                }
            }
        };
        return $this->containerRepository->getList($data);
    }

    public function delete($id)
    {
        if ($this->positionRepository->existsByColumn('container_id', $id)) {
            return $this->makeResponse(status: 0, message: "You can't delete this container. Because it is in the area");
        }
        return $this->containerRepository->delete($id);
    }

    public function fileUpload($data)
    {
        $companies = $this->companyRepository->companiesWithCode();
        $headers = [
            'NAME' => 'number',
            'TYPE' => 'type',
            'STATUS' => 'status',
            'INVOICE' => 'invoice_number',
            'BL_NO' => 'bl_number',
            'SHIPPING_DATE' => 'shipping_date',
            'ETA' => 'eta',
            'ATA' => 'ata',
            'SHIPPER' => 'shipper_id',
            'FORWARDER' => 'forwarder_id',
            'CONSIGNE' => 'consignee_id',
        ];
        try {
            $reader = new Reader();
            $reader->open($data['file']);

            $this->withTransaction(
                executer: function () use ($reader, $headers, $companies) {
                    foreach ($reader->getSheetIterator() as $sheet) {
                        $rowsCount = 0;
                        $columns = [];
                        foreach ($sheet->getRowIterator() as $row) {
                            $rowsCount++;
                            $rowData = $row->toArray();
                            if ($rowsCount == 1) {
                                foreach ($rowData as $h) $columns[] = $headers[$h];
                            } else {
                                $count = count($rowData);
                                $item = [];
                                for ($i = 0; $i < $count; $i++) {
                                    $data = $rowData[$i];
                                    if (isset($columns[$i]) && in_array($columns[$i], ['shipper_id', 'forwarder_id', 'consignee_id'])) {
                                        if (isset($companies[$data])) $data = $companies[$data];
                                        else throw new Error("Company code is incorrect $data", 400);
                                    }
                                    $item[$columns[$i]] = $data;
                                }
                                $this->containerRepository->createWithThrow(data: $item);
                            }
                        }
                    }
                    $this->setResponseData(status: 0, message: "Containers are saved successfully !");
                    $reader->close();
                }, catch: function ($error, $code) use ($reader) {
                    $this->setResponseData(status: 0, message: $error, code: $code);
                    $reader->close();
                }
            );
        } catch (\Throwable $th) {
            return $this->makeResponse(status: 0, message: $th->getMessage(), code: 500);
        }
        return $this->response();
    }
}
