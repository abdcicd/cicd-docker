<?php

namespace App\Services;

use App\Repositories\AreaRepository;
use App\Repositories\GridRepository;
use Illuminate\Support\Facades\Schema;

class GridService extends BaseService
{
    public function __construct(
        protected GridRepository $gridRepository,
        protected AreaRepository $areaRepository,

    ) {
    }

    public function getList($data)
    {
        $data['rows'] = 1000;
        return $this->gridRepository->getList($data);
    }

    public function create($data)
    {
        $lastGrid = $this->gridRepository->getLastGrid($data['area_id']);
        $area = $this->areaRepository->findById($data['area_id']);
        if ($lastGrid->y_axis == 10) {
            $data['x_axis'] = $lastGrid->x_axis + 1;
            $data['y_axis'] = 1;
        } else {
            $data['x_axis'] = $lastGrid->x_axis;
            $data['y_axis'] = $lastGrid->y_axis + 1;
        }
        $area->x = $data['x_axis'];
        $area->save();
        $data['name'] = $area->name . "." . $data['x_axis'] . "." . $data['y_axis'];
        return $this->gridRepository->create($data);
    }

    public function delete($id)
    {
        if ($this->gridRepository->positionExists($id)) {
            return $this->makeResponse(status: 0, message: "You can't delete this grid. Because container exists in this grid");
        }
        return $this->gridRepository->delete($id);
    }

    public function show($id)
    {
        $this->gridRepository->willParseToRelation = [
            'positions' => [
               'grid_name', 'container_id',
                'container' => [
                    'id', 'number'
                ]
            ]
        ];
        return $this->gridRepository->show($id);
    }
}
