<?php

namespace App\Services;

use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\Http\Controllers\AccessTokenController;
use Psr\Http\Message\ServerRequestInterface;

class AuthService extends BaseService
{
    public function __construct(
        protected AccessTokenController $accessTokenController,
        protected UserRepository $userRepository
    ) {
    }

    public function login(ServerRequestInterface $serverRequest, Request $request)
    {
        if (!Auth::attempt($request->only(['username', 'password']))) {
            $this->setResponseData(status: 0, message: "This user not found", code: 404);
        } else {
            $token = $this->accessTokenController->issueToken($serverRequest)->content();
            $this->setResponseData(data: json_decode($token));
        }
        return $this->response();
    }

    public function adUsers($data)
    {
        $adUsers = $this->userRepository->adUsers();
        $this->setResponseData(data: $adUsers);
        return $this->response();
    }

    public function attachRole($id, $data)
    {
        $user = $this->userRepository->findById($id);
        if ($user) {
            $user->roles()->attach($data['role_name']);
            $this->setResponseData(data: $this->userRepository->toResource($user));
        } else
            $this->setResponseData(status: 0, message: 'Not found', code: 404);
        return $this->response();
    }

    public function detachRole($id, $data)
    {
        $user = $this->userRepository->findById($id);
        if ($user) {
            $user->roles()->detach($data['role_name']);
            $this->setResponseData(data: $this->userRepository->toResource($user));
        } else
            $this->setResponseData(status: 0, message: 'Not found', code: 404);
        return $this->response();
    }

    public function syncRoles($id, $data)
    {
        $user = $this->userRepository->findById($id);
        if ($user) {
            $user->roles()->sync($data['roles']);
            $this->setResponseData(data: $this->userRepository->toResource($user));
        } else
            $this->setResponseData(status: 0, message: 'Not found', code: 404);
        return $this->response();
    }
}
