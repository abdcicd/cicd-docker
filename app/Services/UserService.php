<?php

namespace App\Services;

use App\Repositories\UserRepository;

class UserService extends BaseService
{
    public function __construct(
        protected UserRepository $userRepository
    ) {
    }

    public function create($data)
    {
        if($this->userRepository->usernameExists($data['username'])) {
            return $this->makeResponse(status: 0, message: "This username not found in AD server");
        } else return $this->userRepository->create($data);
    }

    public function profile()
    {
        $this->userRepository->willParseToRelation = ['roles' => ['name', 'permissions' => ['name']]];
        return $this->userRepository->show(auth()->user()->uuid);
    }
    
    public function show($id)
    {
        $this->userRepository->willParseToRelation = ['roles' => ['name', 'permissions' => ['name']]];
        return $this->userRepository->show($id);
    }

    public function getList($data)
    {
        $this->userRepository->willParseToRelation = ['roles' => ['name', 'permissions' => ['name']]];
        return $this->userRepository->getList($data);
    }

    public function edit($id, $data)
    {
        unset($data['username']);
        return $this->userRepository->edit($id, $data);
    }
}
