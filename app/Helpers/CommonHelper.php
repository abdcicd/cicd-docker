<?php
namespace App\Helpers;

use App\Enums\PermissionTypeEnum;
use Illuminate\Routing\Route;
use Illuminate\Support\Str;

class CommonHelper 
{
    public static function actionPermission($route = null)
    {
        $route = $route ?? request()->route();
        $permission =  self::getPermission($route);
        $permissionType = PermissionTypeEnum::PERMISSION_TYPE_SYSTEM;

        return compact('permission', 'permissionType');
    }

    public static function getPermission($route)
    {
        try {
            $fullActionNameArr = explode("\\", $route->getActionName());
            list($controller, $action) = explode('@', end($fullActionNameArr));
            $controller = Str::remove('Controller', $controller);
            return  Str::snake($controller) . '-' . Str::snake($action);
            //code...
        } catch (\Throwable $th) {
            dd($th->getMessage(), $fullActionNameArr);
        }
    }

    public static function isCommonPermission($permission)
    {
        return in_array($permission, config('common.common_permissions'));
    }
    
    public static function notNeedRoute($route)
    {
        $notNeedRoutes = config('common.not_need_routes');

        foreach ($notNeedRoutes as $r) {
            if (str_contains($route, $r)) return true;
        }

        return false;
    }
    
    public static function notNeedRouteForTesting($route)
    {
        $notNeedRoutes = config('common.not_need_routes_for_testing');

        foreach ($notNeedRoutes as $r) {
            if (str_contains($route, $r)) return true;
        }

        return false;
    }

}