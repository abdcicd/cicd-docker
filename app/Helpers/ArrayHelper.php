<?php

namespace App\Helpers;

class ArrayHelper
{
    public static function inLike($string, $arr)
    {
        $result = false;

        foreach ($arr as $r) {
            $result = str_contains($string, $r);
        }

        return $result;
    }

    public static function getFirstLetter($word): string
    {

        $consonantDigraphs = ['sh', 'ch', 'ng'];

        $loweredWord = mb_substr(mb_strtolower($word), 0, 2);

        return in_array($loweredWord, $consonantDigraphs) ? mb_substr($word, 0, 2) : mb_substr($word, 0, 1);

    }
}