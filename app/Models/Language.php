<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Language extends BaseModel
{
    use SoftDeletes;

    protected $fillable = [
        'code',
        'name',
        'is_active',
        'created_by',
        'updated_by',
        'deleted_by'
    ];
}
