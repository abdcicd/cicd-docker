<?php

namespace App\Models;

use App\Traits\NewFactoryTrait;
use Database\Factories\GridFactory;

class Grid extends BaseModel
{
	use NewFactoryTrait;

	protected static string $model_factory = GridFactory::class;

    protected $guarded = [];

    protected $fillable = [
        'id',
        'name',
        'area_id',
        'x_axis',
        'y_axis',
        'created_at',
        'updated_at',
        'created_by',
        'updated_by'
    ];

    public function positions()
    {
        return $this->hasMany(Position::class, 'grid_name', 'name');
    }

    public function area()
    {
        return $this->belongsTo(Area::class, 'area_id', 'id');
    }
}