<?php

namespace App\Models;

use App\Traits\NewFactoryTrait;
use Database\Factories\RoleTranslationFactory;

class RoleTranslation extends BaseTranslation
{
    use NewFactoryTrait;

    protected static string $model_factory = RoleTranslationFactory::class;

    protected $table = 'role_translations';

    public $timestamps = false;

    protected $guarded = [];
}
