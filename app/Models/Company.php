<?php

namespace App\Models;

use App\Traits\NewFactoryTrait;
use Database\Factories\CompanyFactory;

class Company extends BaseModel
{
	use NewFactoryTrait;

	protected static string $model_factory = CompanyFactory::class;

    protected $guarded = [];

    protected $fillable = [
        'id',
        'name',
        'code',
        'type',
        'created_at',
        'updated_at',
        'created_by',
        'updated_by'
    ];

    public function shipperContainers()
    {
        return $this->hasMany(Container::class, 'shipper_id', 'id');
    }
    
    public function forwarderContainers()
    {
        return $this->hasMany(Container::class, 'forwarder_id', 'id');
    }

    public function consigneeContainers()
    {
        return $this->hasMany(Container::class, 'consignee_id', 'id');
    }

    public function admin()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }
}