<?php

namespace App\Models;

use Adldap\Laravel\Traits\HasLdapUser;
use App\Traits\HasPermissionsTrait;
use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasUuid, SoftDeletes, HasFactory, Notifiable, HasPermissionsTrait, HasLdapUser;

    public function username()
    {
        return 'username';
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'username',
        'email',
        'firstname',
        'lastname',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    public function findAndValidateForPassport($username, $password)
    {
        $user = $this->where('username', $username)->first();
        // if ($user && !Hash::check($password, $user->password)) {
        //     return false;
        // }
        return $user;
    }
}
