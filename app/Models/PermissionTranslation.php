<?php

namespace App\Models;

use App\Models\BaseTranslation;

class PermissionTranslation extends BaseTranslation
{
    protected $table = 'permission_translations';

    public $timestamps = false;

    protected $guarded = [];
}
