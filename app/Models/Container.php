<?php

namespace App\Models;

use App\Traits\NewFactoryTrait;
use Database\Factories\ContainerFactory;

class Container extends BaseModel
{
	use NewFactoryTrait;

	protected static string $model_factory = ContainerFactory::class;

    protected $guarded = [];

    protected $fillable = [
        'id',
        'number',
        'type',
        'status',
        'invoice_number',
        'bl_number',
        'shipping_date',
        'eta',
        'ata',
        'shipper_id',
        'forwarder_id',
        'consignee_id',
        'created_at',
        'updated_at',
        'created_by',
        'updated_by'
    ];

    public function actions()
    {
        return $this->hasMany(Action::class, 'container_id', 'id');
    }

    public function position()
    {
        return $this->hasOne(Position::class, 'container_id', 'id');
    }

    public function shipper()
    {
        return $this->belongsTo(Company::class, 'shipper_id', 'id');
    }

    public function forwarder()
    {
        return $this->belongsTo(Company::class, 'forwarder_id', 'id');
    }

    public function consignee()
    {
        return $this->belongsTo(Company::class, 'consignee_id', 'id');
    }
}