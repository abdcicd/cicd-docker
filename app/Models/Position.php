<?php

namespace App\Models;

use App\Traits\NewFactoryTrait;
use Database\Factories\PositionFactory;

class Position extends BaseModel
{
    use NewFactoryTrait;

    protected static string $model_factory = PositionFactory::class;

    protected $guarded = [];

    protected $fillable = [
        'grid_name',
        'z_axis',
        'container_id',
        'created_by',
        'updated_by',
    ];

    public function container()
    {
        return $this->hasOne(Container::class, 'id', 'container_id');
    }

    public function grid()
    {
        return $this->belongsTo(Grid::class, 'grid_name', 'name');
    }
}
