<?php

namespace App\Models;

use App\Traits\NewFactoryTrait;
use Database\Factories\AreaFactory;

class Area extends BaseModel
{
	use NewFactoryTrait;

	protected static string $model_factory = AreaFactory::class;

    protected $guarded = [];

    protected $fillable = [
        'id',
        'name',
        'x',
        'y',
        'buffer',
        'limit',
        'description',
        'created_at',
        'updated_at',
        'created_by',
        'updated_by'
    ];
    
    public function grids()
    {
        return $this->hasMany(Grid::class, 'area_id', 'id');
    }
}