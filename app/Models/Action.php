<?php

namespace App\Models;

use App\Traits\NewFactoryTrait;
use Database\Factories\ActionFactory;

class Action extends BaseModel
{
	use NewFactoryTrait;

	protected static string $model_factory = ActionFactory::class;

    protected $guarded = [];

    protected $fillable = [
        'container_id',
        'container_number',
        'container_type',
        'container_status',
        'shipper_name',
        'forwarder_name',
        'consignee_name',
        'user_name',
        'action',
        'position',
        'old_position',
        'created_at',
        'created_by',
    ];

    public function container()
    {
        return $this->belongsTo(Container::class, 'container_id', 'id');
    }
}