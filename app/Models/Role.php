<?php

namespace App\Models;

use App\Traits\HasTranslation;
use App\Traits\NewFactoryTrait;
use Database\Factories\RoleFactory;

class Role extends BaseModel
{
	use HasTranslation, NewFactoryTrait;

	protected static string $model_factory = RoleFactory::class;
	public $translationClass = RoleTranslation::class;

    protected $table = 'roles';

    public $codeField = 'name';

    protected $fillable = [
        'name',
        'type',
        'created_by',
        'updated_by',
    ];

    public function users()
    {
        return $this->belongsToMany(User::class, 'role_user', 'role_name', 'user_id', 'name', 'id');
    }
    
    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'permission_role', 'role_name', 'permission_name', 'name', 'name');
    }
}