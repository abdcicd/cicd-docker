<?php

namespace App\Models;

use App\Traits\HasTranslation;
use App\Traits\NewFactoryTrait;
use Database\Factories\PermissionFactory;

class Permission extends BaseModel
{
	use HasTranslation, NewFactoryTrait;

	protected static string $model_factory = PermissionFactory::class;
	public $translationClass = PermissionTranslation::class;

    protected $guarded = [];

    protected $fillable = [
        'id',
        'name',
        'type',
        'created_at',
        'updated_at',
        'created_by',
        'updated_by'
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'permission_role', 'permission_name', 'role_name', 'name', 'name');
    }
}