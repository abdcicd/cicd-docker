<?php

namespace App\Http\Controllers;

use App\Services\AreaService;
use App\Http\Requests\IndexRequest;
use App\Http\Requests\AreaUpsertRequest;

class AreaController extends Controller
{
    public function __construct(
        protected AreaService $areaService
    ) {
    }

    public function index(IndexRequest $indexRequest)
    {
        return $this->areaService->getList($indexRequest->validated());
    }

    public function store(AreaUpsertRequest $upsertRequest)
    {
        return $this->areaService->create($upsertRequest->validated());
    }

    public function show($id)
    {
        return $this->areaService->show($id);
    }

    public function update($id, AreaUpsertRequest $upsertRequest)
    {
        return $this->areaService->edit($id, $upsertRequest->validated());
    }

    public function destroy($id)
    {
        return $this->areaService->delete($id);
    }
}
