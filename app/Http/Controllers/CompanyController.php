<?php

namespace App\Http\Controllers;

use App\Services\CompanyService;
use App\Http\Requests\IndexRequest;
use App\Http\Requests\CompanyUpsertRequest;

class CompanyController extends Controller
{

    public function __construct(
        protected CompanyService $companyService
    ) {
    }

    public function index(IndexRequest $indexRequest)
    {
        return $this->companyService->getList($indexRequest->validated());
    }

    public function store(CompanyUpsertRequest $upsertRequest)
    {
        return $this->companyService->create($upsertRequest->validated());
    }

    public function show($id)
    {
        return $this->companyService->show($id);
    }

    public function update($id, CompanyUpsertRequest $upsertRequest)
    {
        return $this->companyService->edit($id, $upsertRequest->validated());
    }

    public function destroy($id)
    {
        return $this->companyService->delete($id);
    }
}
