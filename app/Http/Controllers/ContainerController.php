<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContainerMoveRequest;
use App\Services\ContainerService;
use App\Http\Requests\IndexRequest;
use App\Http\Requests\ContainerUpsertRequest;
use App\Http\Requests\ContainerInputRequest;
use App\Http\Requests\FileUploadRequest;
use App\Services\PositionService;

class ContainerController extends Controller
{
    public function __construct(
        protected ContainerService $containerService,
        protected PositionService $positionService
    ) {
    }

    public function input(ContainerInputRequest $containerInputRequest)
    {
        return $this->positionService->input($containerInputRequest->validated());
    }

    public function move($connumber, ContainerMoveRequest $containerMoveRequest)
    {
        return $this->positionService->move($connumber, $containerMoveRequest->validated());
    }

    public function output($connumber)
    {
        return $this->positionService->output($connumber);
    }

    public function matrix()
    {
        return $this->containerService->matrix();
    }

    public function index(IndexRequest $indexRequest)
    {
        return $this->containerService->getList($indexRequest->validated());
    }

    public function store(ContainerUpsertRequest $upsertRequest)
    {
        return $this->containerService->create($upsertRequest->validated());
    }

    public function show($connumber)
    {
        return $this->containerService->show($connumber);
    }

    public function update($connumber, ContainerUpsertRequest $upsertRequest)
    {
        return $this->containerService->edit($connumber, $upsertRequest->validated());
    }

    public function destroy($connumber)
    {
        return $this->containerService->delete($connumber);
    }

    public function outList(IndexRequest $indexRequest)
    {
        return $this->containerService->outList($indexRequest->validated());
    }

    public function uploadData(FileUploadRequest $fileUploadRequest)
    {
        return $this->containerService->fileUpload($fileUploadRequest->validated());
    }
}
