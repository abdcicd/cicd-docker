<?php

namespace App\Http\Controllers;

use App\Services\ActionService;
use App\Http\Requests\IndexRequest;

class ActionController extends Controller
{
    public function __construct(
        protected ActionService $actionService
    ) {
    }

    public function index(IndexRequest $indexRequest)
    {
        return $this->actionService->getList($indexRequest->validated());
    }
}
