<?php

namespace App\Http\Controllers;

use App\Services\RoleService;
use App\Http\Requests\IndexRequest;
use App\Http\Requests\RoleSyncPermissionsRequest;
use App\Http\Requests\RoleUpsertRequest;

class RoleController extends Controller
{

    public function __construct(
        protected RoleService $roleService
    ) {
    }

    public function index(IndexRequest $indexRequest)
    {
        return $this->roleService->getList($indexRequest->validated());
    }

    public function store(RoleUpsertRequest $upsertRequest)
    {
        return $this->roleService->create($upsertRequest->validated());
    }

    public function show($id)
    {
        return $this->roleService->show($id);
    }

    public function update($id, RoleUpsertRequest $upsertRequest)
    {
        return $this->roleService->edit($id, $upsertRequest->validated());
    }

    public function destroy($id)
    {
        return $this->roleService->delete($id);
    }

    public function syncPermissions($id, RoleSyncPermissionsRequest $syncPermissionsRequest)
    {
        return $this->roleService->syncPermissions($id, $syncPermissionsRequest->validated());
    }
}
