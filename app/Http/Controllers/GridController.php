<?php

namespace App\Http\Controllers;

use App\Services\GridService;
use App\Http\Requests\IndexRequest;
use App\Http\Requests\GridUpsertRequest;

class GridController extends Controller
{

    public function __construct(
        protected GridService $gridService
    ) {
    }

    public function index(IndexRequest $indexRequest)
    {
        return $this->gridService->getList($indexRequest->validated());
    }

    public function store(GridUpsertRequest $upsertRequest)
    {
        return $this->gridService->create($upsertRequest->validated());
    }

    public function show($gridname)
    {
        return $this->gridService->show($gridname);
    }

    public function update($gridname, GridUpsertRequest $upsertRequest)
    {
        return $this->gridService->edit($gridname, $upsertRequest->validated());
    }

    public function destroy($gridname)
    {
        return $this->gridService->delete($gridname);
    }
}
