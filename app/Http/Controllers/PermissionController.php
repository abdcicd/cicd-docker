<?php

namespace App\Http\Controllers;

use App\Http\Requests\IndexRequest;
use App\Http\Requests\PermissionUpsertRequest;
use App\Services\PermissionService;

class PermissionController extends Controller
{

    public function __construct(
        protected PermissionService $permissionService
    ) {
    }

    public function index(IndexRequest $indexRequest)
    {
        return $this->permissionService->getList($indexRequest);
    }

    public function show($id)
    {
        return $this->permissionService->show($id);
    }

    public function update($id, PermissionUpsertRequest $upsertRequest)
    {
        return $this->permissionService->edit($id, $upsertRequest->validated());
    }
}
