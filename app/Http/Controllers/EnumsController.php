<?php

namespace App\Http\Controllers;

use App\Services\EnumsService;

class EnumsController extends Controller
{
    public function __construct(protected EnumsService $enumsService)
    {
    }

    public function containerTypes()
    {
        return $this->enumsService->containerTypes();
    }

    public function containerStatusTypes()
    {
        return $this->enumsService->containerStatusTypes();
    }

    public function permissionTypes()
    {
        return $this->enumsService->permissionTypes();
    }

    public function companyTypes()
    {
        return $this->enumsService->companyTypes();
    }
}
