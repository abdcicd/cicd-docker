<?php

namespace App\Http\Controllers;

use App\Http\Requests\IndexRequest;
use App\Http\Requests\UserCreateUpdateRequest;
use App\Services\UserService;

class UserController extends Controller
{
    public function __construct(
        protected UserService $userService
    ) {
    }

    public function profile()
    {
        return $this->userService->profile();
    }
    
    public function index(IndexRequest $indexRequest)
    {
        return $this->userService->getList($indexRequest->validated());
    }

    public function store(UserCreateUpdateRequest $userCreateUpdateRequest)
    {
        return $this->userService->create($userCreateUpdateRequest->validated());
    }

    public function show($id)
    {
        return $this->userService->show($id);
    }

    public function update($id, UserCreateUpdateRequest $userCreateUpdateRequest)
    {
        return $this->userService->edit($id, $userCreateUpdateRequest->validated());
    }

    public function destroy($id)
    {
        return $this->userService->delete($id);
    }
}
