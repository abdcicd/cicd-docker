<?php

namespace App\Http\Controllers;

use App\Http\Requests\IndexRequest;
use App\Http\Requests\UserAttachDetachRoleRequest;
use App\Http\Requests\UserSyncRolesRequest;
use App\Services\AuthService;
use Illuminate\Http\Request;
use Psr\Http\Message\ServerRequestInterface;

class AuthController extends Controller
{
    public function __construct(
        protected AuthService $authService
    ) {
    }

    public function adUsers(IndexRequest $indexRequest)
    {
        return $this->authService->adUsers($indexRequest->validated());
    }

    public function login(ServerRequestInterface $serverRequest, Request $request)
    {
        return $this->authService->login($serverRequest, $request);
    }

    public function attachRole($id, UserAttachDetachRoleRequest $userAttachDetachRoleRequest)
    {
        return $this->authService->attachRole($id, $userAttachDetachRoleRequest->validated());
    }

    public function detachRole($id, UserAttachDetachRoleRequest $userAttachDetachRoleRequest)
    {
        return $this->authService->detachRole($id, $userAttachDetachRoleRequest->validated());
    }

    public function syncRoles($id, UserSyncRolesRequest $userSyncRolesRequest)
    {
        return $this->authService->syncRoles($id, $userSyncRolesRequest);
    }
}
