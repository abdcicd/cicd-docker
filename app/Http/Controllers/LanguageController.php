<?php

namespace App\Http\Controllers;

use App\Http\Requests\IndexRequest;
use App\Http\Requests\LanguageStoreUpdateRequest;
use App\Services\LanguageService;

class LanguageController extends Controller
{
    public function __construct(
        protected LanguageService $languageService
    ) {
    }

    public function list(IndexRequest $indexRequest)
    {
        return $this->languageService->list($indexRequest->validated());
    }

    public function index(IndexRequest $indexRequest)
    {
        return $this->languageService->getList($indexRequest->validated());
    }

    public function store(LanguageStoreUpdateRequest $languageStoreUpdateRequest)
    {
        return $this->languageService->create($languageStoreUpdateRequest->validated());
    }

    public function show($id)
    {
        return $this->languageService->show($id);
    }

    public function update($id, LanguageStoreUpdateRequest $languageStoreUpdateRequest)
    {
        return $this->languageService->edit($id, $languageStoreUpdateRequest->validated());
    }

    public function destroy($id)
    {
        return $this->languageService->softDelete($id);
    }
}
