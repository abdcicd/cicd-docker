<?php

namespace App\Http\Resources;

class PositionResource extends BaseResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'grid_name' => $this->grid_name,
            'z_axis' => $this->z_axis,
            'container_number' => $this->container_number,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ];
    }
}
