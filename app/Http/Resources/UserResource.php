<?php

namespace App\Http\Resources;

class UserResource extends BaseResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'uuid' => $this->uuid,
            'username' => $this->username,
            'firstname' => $this->firstname,
            'lastname' => $this->lastname,
            'roles' => $this->roles->map(fn($r) => $r->name),
            'permissions' => $this->collectPermissions()
        ];
    }

    public function collectPermissions()
    {
        $permissions = collect();
        foreach ($this->roles as $r) foreach($r->permissions as $p) $permissions->push($p->name);
        return $permissions;
    }
}
