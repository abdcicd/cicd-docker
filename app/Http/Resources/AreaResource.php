<?php

namespace App\Http\Resources;

class AreaResource extends BaseResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'x' => $this->x,
            'y' => $this->y,
            'limit' => $this->limit,
            'buffer' => $this->buffer,
            'description' => $this->description
        ];
    }
}
