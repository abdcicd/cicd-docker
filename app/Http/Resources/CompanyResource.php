<?php

namespace App\Http\Resources;

use App\Enums\CompanyTypeEnum;
use App\Traits\ChangeResource;

class CompanyResource extends BaseResource
{
    use ChangeResource;

    public function base()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'code' => $this->code,
            'type' => CompanyTypeEnum::getLabel($this->type),
            'type_id' => $this->type,
            'created_at' => $this->created_at,
            'created_by' => $this->admin
        ];
    }
}
