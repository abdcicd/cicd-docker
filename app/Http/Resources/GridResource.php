<?php

namespace App\Http\Resources;

use App\Traits\ChangeResource;

class GridResource extends BaseResource
{
    use ChangeResource;

    public function base()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'area_id' => $this->area_id,
            'x_axis' => $this->x_axis,
            'y_axis' => $this->y_axis,
        ];
    }
    public function show()
    {
        return [
            'containers' => $this->positions->map(fn($p) => $p->container)
        ];
    }
}
