<?php

namespace App\Http\Resources;


class AdUserResource extends BaseResource
{
    public function toArray($request)
    {
        return [
            'username' => is_array($this->samaccountname) ? $this->samaccountname[0] : $this->samaccountname,
            'firstname' => is_array($this->givenname) ? $this->givenname[0] : $this->givenname,
            'lastname' => is_array($this->sn) ? $this->sn[0] : $this->sn,
        ];
    }
}
