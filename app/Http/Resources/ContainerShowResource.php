<?php

namespace App\Http\Resources;

use App\Enums\ContainerStatusEnum;
use App\Enums\ContainerTypeEnum;

class ContainerShowResource extends BaseResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'number' => $this->number,
            'type' => $this->type,
            'type_name' => ContainerTypeEnum::getLabel($this->type),
            // 'type' => ['code' => $this->type, 'name' => ContainerTypeEnum::getLabel($this->type)],
            'status' => $this->status,
            'status_name' => ContainerStatusEnum::getLabel($this->status),
            // 'status' => ['code' => $this->status, 'name' => ContainerStatusEnum::getLabel($this->status)],
            'invoice_number' => $this->invoice_number,
            'bl_number' => $this->bl_number,
            'shipping_date' => $this->shipping_date,
            'eta' => $this->eta,
            'ata' => $this->ata,
            'position' => $this->position,
            'shipper' => $this->shipper,
            'forwarder' => $this->forwarder,
            'consignee' => $this->consignee,
            'actions' => $this->actions,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
