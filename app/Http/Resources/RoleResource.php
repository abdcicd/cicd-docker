<?php

namespace App\Http\Resources;

use Illuminate\Support\Facades\Route;

class RoleResource extends BaseResource
{
    public function toArray($request)
    {
        $short = [
            'id' => $this->id,
            'name' => $this->name,
            'type' => $this->type,
            'translation' => $this->translation->translation ?? '',
            'users' => $this->users->map(function($user){unset($user->pivot);return $user;}),
            'description' => $this->translation->description ?? '',
        ];
        
        $collection = [
            'id' => $this->id,
            'name' => $this->name,
            'type' => $this->type,
            'translation' => $this->translation->translation ?? '',
            'description' => $this->translation->description ?? '',
            'translations' => $this->translations,
            'permissions' => PermissionResource::collection($this->permissions),
            'users' => $this->users->map(function($user){unset($user->pivot);return $user;}),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];

        if (str_contains(Route::currentRouteName(), 'profile')) {
            $collection = $short;
        }

        return $collection;
    }
}
