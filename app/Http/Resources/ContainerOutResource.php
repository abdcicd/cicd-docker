<?php

namespace App\Http\Resources;

class ContainerOutResource extends BaseResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'number' => $this->number,
            'type' => $this->type,
            // 'type' => ['code' => $this->type, 'name' => ContainerTypeEnum::getLabel($this->type)],
            'status' => $this->status,
            // 'status' => ['code' => $this->status, 'name' => ContainerStatusEnum::getLabel($this->status)],
            'shipper' => $this->shipper,
            'forwarder' => $this->forwarder,
            'consignee' => $this->consignee,
            'shipping_date' => $this->shipping_date,
        ];
    }
}
