<?php

namespace App\Http\Resources;

use Illuminate\Support\Facades\Route;

class PermissionResource extends BaseResource
{
    public function toArray($request)
    {
        $full = [
            'id' => $this->id,
            'name' => $this->name,
            'type' => $this->type,
            'translation' => $this->translation->translation ?? '',
            'description' => $this->translation->description ?? '',
            'translations' => $this->translations,

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];

        $short = [
            'id' => $this->id,
            'name' => $this->name,
            'type' => $this->type,
            'translation' => $this->translation?->translation,
            'description' => $this->translation?->description,
        ];

        $collection = $short;

        if (str_contains(Route::currentRouteName(), 'show')) {
            $collection = $full;
        }

        return $collection;
    }
}
