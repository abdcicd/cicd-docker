<?php

namespace App\Http\Resources;

use App\Traits\FilterableResourceTrait;
use Illuminate\Http\Resources\Json\JsonResource;

class BaseResource extends JsonResource
{
    use FilterableResourceTrait;
}
