<?php

namespace App\Http\Resources;

class ActionResource extends BaseResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'container_id' => $this->container_id,
            'container_number' => $this->container_number,
            'container_status' => $this->container_status,
            'container_type' => $this->container_type,
            'shipper_name' => $this->shipper_name,
            'forwarder_name' => $this->forwarder_name,
            'consignee_name' => $this->consignee_name,
            'user_name' => $this->user_name,
            'action' => $this->action,
            'position' => $this->position,
            'old_position' => $this->old_position,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
        ];
    }
}
