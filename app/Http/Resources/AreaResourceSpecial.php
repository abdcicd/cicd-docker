<?php

namespace App\Http\Resources;

class AreaResourceSpecial extends BaseResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'x' => $this->x,
            'y' => $this->y,
            'limit' => $this->limit,
            'buffer' => $this->buffer,
            'description' => $this->description,
            'grids' => $this->grids
        ];
    }
}
