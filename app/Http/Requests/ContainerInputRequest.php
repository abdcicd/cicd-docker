<?php

namespace App\Http\Requests;

use App\Enums\ContainerStatusEnum;
use App\Enums\ContainerTypeEnum;
use App\Rules\CompanyExists;
use App\Rules\ContainerExists;
use App\Rules\GridExists;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ContainerInputRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'grid_name' => ['required', 'string', new GridExists('name')],
            'container_id' => ['nullable', 'integer', new ContainerExists],
            'status' => ['required', 'integer', ContainerStatusEnum::in()],
            'number' => ['nullable', 'string', Rule::unique('containers')]
        ];
    }
}
