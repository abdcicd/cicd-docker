<?php

namespace App\Http\Requests;

use App\Enums\CompanyTypeEnum;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CompanyUpsertRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $ignore = $this->route('company');
        $required = ($this->isMethod('put')) ? 'nullable' : 'required';
        
        return [
            'name' => [$required, 'string', Rule::unique('companies')->ignore($ignore)],
            'type' => [$required, 'integer', CompanyTypeEnum::in()],
            'code' => [$required, 'string', Rule::unique('companies')->ignore($ignore)]
        ];
    }
}
