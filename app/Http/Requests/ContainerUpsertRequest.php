<?php

namespace App\Http\Requests;

use App\Enums\ContainerStatusEnum;
use App\Enums\ContainerTypeEnum;
use App\Rules\CompanyExists;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ContainerUpsertRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $required = ($this->isMethod('put')) ? 'nullable' : 'required';
        return [
            'number' => [$required, 'string'],
            'type' => [$required, 'integer', ContainerTypeEnum::in()],
            'status' => [$required, 'integer', ContainerStatusEnum::in()],
            'invoice_number' => [$required, 'string'],
            'bl_number' => [$required, 'string'],
            'shipping_date' => [$required, 'string'],
            'eta' => [$required, 'string'],
            'ata' => [$required, 'string'],
            'shipper_id' => [$required, 'integer', new CompanyExists],
            'forwarder_id' => [$required, 'integer', new CompanyExists],
            'consignee_id' => [$required, 'integer', new CompanyExists]
        ];
    }
}
