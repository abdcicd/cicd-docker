<?php

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AreaUpsertRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $required = ($this->isMethod('put')) ? 'nullable' : 'required';
        $id = $this->route('area');
        return [
            'name' => [$required, 'string', Rule::unique('areas', 'name')->ignore($id, 'id')],
            'x' => [$required, 'integer'],
            'y' => [$required, 'integer'],
            'buffer' => ['nullable', 'integer'],
            'limit' => ['nullable', 'integer'],
            'description' => ['nullable', 'string']
        ];
    }
}
