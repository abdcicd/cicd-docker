<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserCreateUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('user');
        $required = ($this->isMethod('put')) ? 'nullable' : 'required';
        $unique = Rule::unique('users')->where(function ($q) {
            $q->whereNull('deleted_at');
        })->ignore($id, 'id');
        
        return [
            'username' => [$required, 'string', $unique],
            'firstname' => ['nullable', 'string', 'max:200'],
            'lastname' => ['nullable', 'string', 'max:200'],
        ];
    }
}
