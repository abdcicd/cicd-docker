<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class LanguageStoreUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('language');
        $required = 'required';
        $unique = Rule::unique('languages')->withoutTrashed();
        if (request()->isMethod('PUT')) {
            $required = 'nullable';
            $unique = $unique->ignore($id);
        }

        return [
            'name' => [$required, $unique, 'string'],
            'code' => [$required, $unique, 'string'],
            'is_active' => ['integer', 'nullable'],
        ];
    }
}
