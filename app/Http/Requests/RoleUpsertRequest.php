<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class RoleUpsertRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('role');
        $name = ['required', 'string'];
        
        $required = ($this->isMethod('put')) ? 'nullable' : 'required';

        if (request()->isMethod('POST')) {
            $name = ['required', 'string', Rule::unique('roles')];
        } elseif (request()->isMethod('PUT')) {
            $name = ['nullable', 'string', Rule::unique('roles')->ignore($id)];
        }
        return [
            'name' => $name,
            'type' => ['nullable'],
            'translations' => ['required', 'array'],
            'translations.*' => ['required', 'array'],
            'translations.*.id' => ['nullable'],
            'translations.*.translation' => ['required', 'string'],
            'translations.*.description' => ['required', 'string'],
            'translations.*.language_code' => [$required, 'string'],
        ];
    }
}
