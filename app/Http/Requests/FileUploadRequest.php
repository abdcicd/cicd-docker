<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class FileUploadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file' => [
                'required',
                'file',
                'mimes:xls,xlsx',
                'max:4096'
            ],
        ];
    }

    // public function messages()
    // {
    //     return [
    //         'file.mimes' => 'Bu exel file bo\'lishi lozim',
    //         'required' => "Bu_kerak"
    //     ];
    // }

    // protected function failedValidation(Validator $validator)
    // {
    //     $error = $validator->errors()->first();
        
    //     throw new HttpResponseException(response()->json(['message' => $error], 422));
    // }
}
