<?php

namespace App\Http\Requests;

use App\Rules\RoleCheck;
use App\Rules\RoleExists;
use Illuminate\Foundation\Http\FormRequest;

class UserSyncRolesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'roles' => ['present', 'array'],
            'roles.*' => ['nullable', 'string', new RoleExists, new RoleCheck],
        ];
    }
}
