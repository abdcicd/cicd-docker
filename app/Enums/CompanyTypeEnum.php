<?php

namespace App\Enums;

class CompanyTypeEnum extends BaseEnum 
{
    const FORWARDER = 10;
    const SHIPPER = 20;
    const CONSIGNEE = 30;

    public static function list($withText = false)
    {
        return ($withText) ? [
            self::FORWARDER => 'Forwarder',
            self::SHIPPER => 'Shipper',
            self::CONSIGNEE => 'Consignee',
        ] : [self::FORWARDER, self::SHIPPER, self::CONSIGNEE];
    }
}