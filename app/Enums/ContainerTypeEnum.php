<?php

namespace App\Enums;

class ContainerTypeEnum extends BaseEnum
{
    const TWENTY = 20;
    const FORTY = 40;

    public static $t = [
        'twenty' => 'Yigirma',
        'forty' => 'Qirq'
    ];

    public static function list($withText = false)
    {
        return ($withText) ? [
            self::TWENTY => self::$t['twenty'],
            self::FORTY => self::$t['forty']
        ] : [
            self::TWENTY,
            self::FORTY
        ];
    }
}
