<?php

namespace App\Enums;

class ContainerStatusEnum extends BaseEnum
{
    const FULL = 10;
    const EMPTY = 20;
    const HALF = 30;

    public static $t = [
        'full' => 'To\'la',
        'empty' => 'Bo\'sh',
        'half' => 'Yarim to\'la'
    ];

    public static function list($withText = false)
    {
        return ($withText) ? [
            self::FULL => self::$t['full'],
            self::EMPTY => self::$t['empty'],
            self::HALF => self::$t['half'],
        ] : [
            self::FULL,
            self::EMPTY,
            self::HALF,
        ];
    }
}
