<?php

namespace App\Enums;

class PermissionTypeEnum extends BaseEnum 
{
    const PERMISSION_TYPE_SUPERADMIN = 1;
    const PERMISSION_TYPE_SYSTEM = 2;
    const PERMISSION_TYPE_USER = 3;

    public static function list($withText = false)
    {
        return ($withText) ? [
            self::PERMISSION_TYPE_SUPERADMIN => 'superadmin',
            self::PERMISSION_TYPE_SYSTEM => 'system',
            self::PERMISSION_TYPE_USER => 'user',
        ] : [self::PERMISSION_TYPE_SUPERADMIN, self::PERMISSION_TYPE_SYSTEM, self::PERMISSION_TYPE_USER];
    }
}